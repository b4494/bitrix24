<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\Features\SupportsChatsImport;
use BmPlatform\Abstraction\Interfaces\Features\SupportsTemporaryData;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Bitrix24\Presenters\ChatPresenter;
use BmPlatform\Bitrix24\Presenters\MessengerInstancePresenter;
use BmPlatform\Bitrix24\Presenters\OperatorPresenter;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class AppHandler implements AppHandlerInterface, HasOperators, SupportsTemporaryData
{
    public function __construct(
        protected readonly AppInstance $appInstance,
        protected Repository           $config,
        protected CredentialsRepo      $credentialsRepo,
        private ?ApiCommands           $apiCommands = null,
        private ?Commands              $commands = null,
        private ?ApiClient             $apiClient = null,
    ) {
        //
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        $openLines = Collection::make($this->getApiCommands()->getActiveOpenLines([ 'ID', 'LINE_NAME' ]))->keyBy->ID;

        $requests = [];

        $connectors = $this->getAvailableConnectors();

        foreach ($openLines as $openLine) {
            foreach ($connectors as $connector => $type) {
                $requests["{$openLine->ID}_$connector"] = [
                    'method' => 'imconnector.status',
                    'query' => [ 'LINE' => $openLine->ID, 'CONNECTOR' => $connector ],
                ];
            }
        }

        $result = [];

        foreach (array_chunk($requests, 50, true) as $chunk) {
            foreach ($this->getApiCommands()->batchRequest($chunk) as $key => $statusResult) {
                if (!$statusResult['STATUS']) continue;

                [ $line, $id ] = explode('_', $key, 2);

                $result[] = MessengerInstancePresenter::make($id, $connectors[$id], $openLines[$line]);
            }
        }

        return new IterableData($result, null);
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        return new IterableData(
            array_map(fn (Bitrix24User $user) => $user->toOperator(), $this->getApiCommands()->getEmployeeUsers($cursor)),
            $this->getApiCommands()->getCursor(),
        );
    }

//    public function getChats(mixed $cursor = null): IterableData
//    {
//        $paginator = $this->getApiCommands()->getRecentOpenLineChats($cursor);
//
//        $config_messenger_types = $this->config->get('bitrix24.messenger_types');
//        $domain = $this->getDomain();
//
//        // TODO: fix insane amount of requests to B24
//        return new IterableData(
//            array_map(
//                function (Bitrix24OpenLineChat $chat) use ($config_messenger_types, $domain) {
//                    return ChatPresenter::make(
//                        b24Chat: $chat,
//                        contact: $this->getApiCommands()->getContactByChatUserId($chat->startUserId),
//                        operator: $chat->userCounter > 2 ? $this->getApiCommands()->getChatOperatorById(
//                            $chat->id, $this->getBotId(), $chat->startUserId
//                        ) : null,
//                        domain: $domain,
//                        config_messenger_types: $config_messenger_types
//                    );
//                },
//                $paginator->items
//            ),
//            $paginator->has_more ? $paginator->getCursor() : null
//        );
//    }

    public function getCommands(): CommandsInterface
    {
        if ($this->commands === null) {
            $this->commands = new Commands($this->getApiCommands(), $this->getBotId(), $this->getDomain());
        }

        return $this->commands;
    }

    public function getApiClient(): ApiClient
    {
        if (isset($this->apiClient)) return $this->apiClient;

        return $this->apiClient = new ApiClient([ 'appInstance' => $this->appInstance ]);
    }

    public function getApiCommands(): ApiCommands
    {
        if (isset($this->apiCommands)) return $this->apiCommands;

        return $this->apiCommands = new ApiCommands($this->getOAuthAccessToken(), $this->getApiClient(), $this);
    }

    public function activate(): void
    {
        //
    }

    public function deactivate(): void
    {
        //
    }

    public function freshTemporaryData(TemporaryData $data): TemporaryData
    {
        return OAuthAccessToken::fromTemporaryData($data)
            ->refresh($this->getApiClient(), $this->getOAuthCredentials())
            ->toTemporaryData()
        ;
    }

    public function getAppInstance(): AppInstance
    {
        return $this->appInstance;
    }

    public function getOAuthCredentials(): Bitrix24AppCredentials
    {
        return $this->credentialsRepo->getByCodeOrFail($this->appInstance->getExtraData()[ExtraDataProps::APP_CODE]);
    }

    public function getOAuthAccessToken(bool $forceRefresh = false): OAuthAccessToken
    {
        if (!$data = $this->appInstance->getTemporaryData($forceRefresh)) {
            throw new ErrorException(ErrorCode::AuthenticationFailed, 'Missing temporary data');
        }

        return OAuthAccessToken::fromTemporaryData($data);
    }

    public function getDomain(): string
    {
        return $this->appInstance->getExtraData()[ExtraDataProps::DOMAIN];
    }

    public function getBotId(): int
    {
        return $this->appInstance->getExtraData()[ExtraDataProps::BOT_ID];
    }

    public function status(): AppExternalStatus
    {
        try {
            return $this->getApiCommands()->appInfo()->toAppExternalStatus();
        }

        catch (ErrorException $e) {
            return match ($e->errorCode) {
                ErrorCode::PaymentExpired => new AppExternalStatus(AppStatus::Frozen),
                ErrorCode::AccountDisabled => new AppExternalStatus(AppStatus::Disabled),
                default => throw $e,
            };
        }
    }

    /**
     * @return string
     */
    public function getMemberId(): string
    {
        [$memberId] = explode(':', $this->appInstance->getExternalId());

        return $memberId;
    }

    /** @return Collection<string, string> */
    protected function getAvailableConnectors(): Collection
    {
        $types = collect($this->config->get('bitrix24.messenger_types'))->where('name', '!=', null);

        if ($this->getApiCommands()->checkMethodExists('imconnector.list')) {
            return collect($this->getApiCommands()->getConnectorsList())
                ->map(fn ($v, $k) => $types->has($k) ? $k : $this->matchDynamicType($k))
                ->filter();
        }

        return $types->map(fn ($v, $k) => $k);
    }

    protected function matchDynamicType($k): ?string
    {
        foreach ($this->config->get('bitrix24.dynamic_messenger_types') as $re) {
            if (preg_match($re, $k, $matches)) return $matches['type'];
        }

        return null;
    }
}
