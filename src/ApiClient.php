<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Support\Http\HttpClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class ApiClient extends HttpClient
{
    public function __construct(
        array $options = []
    ) {
        parent::__construct($options);
    }

    private const OAUTH_URL = 'https://oauth.bitrix.info/oauth/token/';

    public function sendRestApiRequest(OAuthAccessToken $token, string $action, array $form_params = []): mixed
    {
        return $this->post(":b24endpoint/$action", array_filter([
            'params' => [
                'b24endpoint' => rtrim($token->clientEndpoint, '/'),
            ],

            'query' => [
                'auth' => $token->accessToken,
            ],

            'form_params' => $form_params,
        ]));
    }

    /**
     * @throws ErrorException
     */
    public function sendOAuthRequest(array $query = []): array
    {
        return parent::get(self::OAUTH_URL, [
            'query' => $query
        ]);
    }

    protected function defaultConfig(): array
    {
        return [
            RequestOptions::CONNECT_TIMEOUT => 2.0,
            RequestOptions::TIMEOUT         => 10.0,
        ];
    }

    protected function processResponse(ResponseInterface $response): mixed
    {
        $content = $response->getBody()->getContents();

        $content_decoded = json_decode($content, true);

        if ($response->getStatusCode() === 200) {
            return $content_decoded;
        } elseif ($response->getStatusCode() === 502) {
            throw new ErrorException(ErrorCode::ServiceUnavailable, 'Service unavailable');
        } elseif ($response->getStatusCode() === 429) {
            throw new ErrorException(ErrorCode::TooManyRequests, 'Too many requests');
        }

        $error             = strtoupper((string)Arr::get($content_decoded, 'error'));
        $error_description = Arr::get($content_decoded, 'error_description', 'Unexpected Error');

        $error_code = match ($error) {
            'EXPIRED_TOKEN'   => ErrorCode::AuthenticationFailed,
            'MAX_COUNT_ERROR' => ErrorCode::IntegrationNotPossible,
            'BOT_ID_ERROR', 'USER_NOT_EXISTS' => ErrorCode::NotFound,
            'ID_EMPTY', 'NAME_ERROR', 'MESSAGE_EMPTY', 'USER_ID_EMPTY', 'CHAT_ID_EMPTY' => ErrorCode::DataMissing,
            'INTERNAL_SERVER_ERROR' => ErrorCode::InternalServerError,
            'PAYMENT_REQUIRED' => ErrorCode::PaymentExpired,
            'NO_AUTH_FOUND', 'INVALID_TOKEN', 'INVALID_GRANT',
            'NOT_INSTALLED', 'ACCESS_DENIED', 'PORTAL_DELETED', 'AUTHORIZATION_ERROR' => ErrorCode::AccountDisabled,
            default                 => ErrorCode::UnexpectedServerError
        };

        throw new ErrorException($error_code, "$error: $error_description");
    }
}
