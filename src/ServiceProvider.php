<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\Bitrix24\Components\ConfigValidator;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\App;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/bitrix24.php', 'bitrix24');

        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');

        $this->loadViewsFrom(__DIR__.'/../views', 'b24');

        /** @var Repository $config */
        $config = $this->app['config'];

        /** @var CredentialsRepo $credentials */
        $credentials = App::make(CredentialsRepo::class);

        $hub = $this->makeHub();

        $hub->registerAppType(
            'b24',
            static fn (AppInstance $app) => new AppHandler($app, $config, $credentials),
            [
                'schema' => static fn () => $config->get('bitrix24'),
                'registerVariables' => $this->registerVariables(),
            ],
        );
    }

    /**
     * @return mixed
     * @throws BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }

    public function register()
    {
        $this->app->singleton(CredentialsRepo::class, fn () => new CredentialsRepo($this->app->make('config')));
    }

    protected function registerVariables(): callable
    {
        return function (VariableRegistrar $registrar) {
            $registrar->int('chat.leadId', static fn (Chat $chat) => $chat->getExtraData()[ExtraDataProps::LEAD_ID] ?? null);
            $registrar->int('chat.dealId', static fn (Chat $chat) => $chat->getExtraData()[ExtraDataProps::DEAL_ID] ?? null);
            $registrar->int('chat.contactId', static fn (Chat $chat) => $chat->getExtraData()[ExtraDataProps::CONTACT_ID] ?? null);
            $registrar->int('chat.companyId', static fn (Chat $chat) => $chat->getExtraData()[ExtraDataProps::COMPANY_ID] ?? null);
            
            $registrar->text('contact.firstName', static fn (Contact $contact) => $contact->getExtraData()[ExtraDataProps::FIRST_NAME] ?? null);
            $registrar->text('contact.lastName', static fn (Contact $contact) => $contact->getExtraData()[ExtraDataProps::LAST_NAME] ?? null);

            $registrar->text('operator.firstName', static fn (Operator $operator) => $operator->getExtraData()[ExtraDataProps::FIRST_NAME] ?? null);
            $registrar->text('operator.lastName', static fn (Operator $operator) => $operator->getExtraData()[ExtraDataProps::LAST_NAME] ?? null);

            $registrar->text('domain', static fn (RuntimeContext $ctx) => $ctx->appInstance()->getExtraData()[ExtraDataProps::DOMAIN]);
            $registrar->text('botId', static fn (RuntimeContext $ctx) => $ctx->appInstance()->getExtraData()[ExtraDataProps::BOT_ID]);
        };
    }
}
