<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsCustomActions;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Bitrix24\Commands\MessageCommands;
use BmPlatform\Bitrix24\Commands\OperatorCommands;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Mime\MimeTypes;

class Commands implements CommandsInterface, SendsSystemMessages, TransfersChatToOperator, SupportsCustomActions
{
    use MessageCommands;
    use OperatorCommands;

    public function __construct(
        protected ApiCommands $api,
        protected int $bot_id,
        protected string $domain
    ) {
    }

//    public function updateContactData(UpdateContactDataRequest $request): void
//    {
//        $this->api->updateUser(
//            new Bitrix24User(
//                id: $request->contact->getExternalId(),
//                firstName: $request->name,
//                phone: $request->phone
//            )
//        );
//
//        $contact = $this->api->getContactByChatUserId($request->contact->getExternalId());
//
//        if ($contact !== null) {
//            $this->api->updateContact(
//                new Bitrix24Contact(
//                    id: $contact->id,
//                    name: $request->name,
//                    emails: array_filter([$request->email]),
//                    phones: array_filter([$request->phone]),
//                    comments: $request->comment
//                )
//            );
//        }
//    }
    public function callCustomAction(RuntimeContext $context, string $id, array $args): ?array
    {
        return match ($id) {
            'restApiCall' => $this->restApiCall($args),
            'leave' => $this->leaveChat($context),
            'publishFiles' => $this->publishFiles($context, $args),
            default => throw new ErrorException(ErrorCode::FeatureNotSupported, "Custom action [$id] is not supported"),
        };
    }

    /**
     * @param array $args
     *
     * @return mixed
     */
    protected function restApiCall(array $args): array
    {
        if (!isset($args['action'])) throw new ErrorException(ErrorCode::UnexpectedData, 'Action must be provided');

        try {
            return [
                'data' => $this->api->sendRestApiRequest($args['action'], $args['query'] ?? []),
                'status' => 'ok',
            ];
        }

        catch (ErrorException $e) {
            if (in_array($e->errorCode, [ ErrorCode::DataMissing, ErrorCode::UnexpectedServerError ])) {
                return [ 'message' => $e->getMessage(), 'status' => $e->errorCode->value ];
            }

            throw $e;
        }
    }

    protected function leaveChat(RuntimeContext $context): mixed
    {
        $this->api->sendRestApiRequest('imbot.chat.leave', [
            'CHAT_ID' => $context->chat()->getExternalId(),
            'BOT_ID' => $this->bot_id,
        ]);

        return null;
    }

    protected function publishFiles(RuntimeContext $context, array $args): array
    {
        if (!isset($args['files'])) {
            throw new ErrorException(ErrorCode::UnexpectedData, 'Files are not specified');
        }

        $urls = [];
        $errors = [];

        foreach ($this->prepareFiles($args['files']) as $index => $url) {
            if (!$url = $this->processFileUrl($url)) {
                $errors[$index] = 'Invalid or missing url';

                continue;
            }

            try {
                $urls[$index] = $this->getPublicFileUrl($context, $url);
            }

            catch (\Exception $e) {
                logger()->error('Error happened when tried to download file', [
                    'appInstance' => $context->appInstance(),
                    'e' => $e,
                    'url' => $url,
                ]);

                $errors[$index] = $e->getMessage();
            }
        }

        return compact('urls', 'errors');
    }

    protected function prepareFiles(mixed $files): array
    {
        if (is_string($files)) return [ $files ];
        if (!is_array($files)) throw new ErrorException(ErrorCode::UnexpectedData, 'Files must be string or array');

        return Arr::pluck(is_array(Arr::first($files)) ? $files : [ $files ], 'showUrl');
    }

    protected function processFileUrl(?string $url): ?string
    {
        if (!$url || !str($url)->contains('show_file.php')) return null;

        return $url;
    }

    protected function getPublicFileUrl(RuntimeContext $context, string $url): ?string
    {
        $handler = $this->api->app;

        $name = md5($handler->getDomain().$url);

        return Cache::remember($name.':1', now()->addWeek(), function () use ($name, $handler, $context, $url) {
            $resp = $this->getFileContents($handler, $url);
            $contentType = $resp->header('content-type');

            if (!$ext = MimeTypes::getDefault()->getExtensions($contentType)[0] ?? null) {
                throw new ErrorException(
                    ErrorCode::InvalidResponseData,
                    'Got unsupported content type ['.$contentType.']',
                );
            }

            $name .= '.'.$ext;

            if ($publicUrl = $context->appInstance()->publishedFileUrl($name)) return $publicUrl;

            try {
                return $context->appInstance()->publishFile($body = $resp->toPsrResponse()->getBody(), $name);
            }

            finally {
                $body?->close();
            }
        });
    }

    protected function getFileContents(AppHandler $handler, string $url, bool $force = false): Response
    {
        $token = $handler->getOAuthAccessToken($force);

        $resp = Http::get(substr($token->clientEndpoint, 0, -6).$url.'&auth='.$token->accessToken);

        $contentType = str($resp->header('content-type'));

        if ($contentType->is('text/html*')) {
            if (!$force) return $this->getFileContents($handler, $url, force: true);

            throw new ErrorException(ErrorCode::InvalidResponseData, 'Unknown file response data');
        } elseif ($contentType->is('application/json*')) {
            throw new ErrorException(ErrorCode::InvalidResponseData, $resp->body());
        }

        return $resp;
    }
}
