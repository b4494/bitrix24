<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use Illuminate\Config\Repository;

class CredentialsRepo
{
    /** @var array<string, Bitrix24AppCredentials> */
    private array $items;

    public function __construct(private readonly Repository $config)
    {
        //
    }

    /**
     * @return Bitrix24AppCredentials[]
     */
    public function getAll(): array
    {
        if (isset($this->items)) return $this->items;

        return $this->items = collect($this->config->get('bitrix24.applications', []))
            ->mapWithKeys(static fn (array $data) => with(Bitrix24AppCredentials::fromArray($data), static fn (Bitrix24AppCredentials $c) => [ $c->id => $c ]))
            ->all();
    }

    public function getByCodeOrFail(string $code): Bitrix24AppCredentials
    {
        $data = $this->getAll();

        if (!isset($data[$code])) throw new ErrorException(ErrorCode::AuthenticationFailed, 'Missing app secret for this member_id');

        return $data[$code];
    }

    public function getDefault(): Bitrix24AppCredentials
    {
        return $this->getAll()['default'];
    }

    public function has(string $code): bool
    {
        return isset($this->getAll()[$code]);
    }
}
