<?php

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AccountType;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use Illuminate\Support\Facades\Config;

class Integrator
{
    public function __construct(
        public readonly Hub                    $hub,
        public readonly Bitrix24AppCredentials $creds,
        public readonly AppDisplayPayload      $data,
        public readonly Bitrix24AppInfo        $appInfo,
    ) {
        //
    }

    public function __invoke(): AppInstance
    {
        $botId = $this->data->getApiCommands()->createBot(
            $botCode = ($this->creds->botCode ?: Config::get('bitrix24.bot.code')),
            $this->creds->botName ?: Config::get('bitrix24.bot.name'),
            Config::get('bitrix24.bot.email'),
            Config::get('bitrix24.bot.url'),
        );

        return $this->hub->integrate($this->makeIntegrationData($botId, $botCode));
    }

    protected function makeIntegrationData($botId, $botCode): AppIntegrationData
    {
        return new AppIntegrationData(
            type: 'b24',
            externalId: $this->data->memberId.':'.$botCode,
            name: $this->data->domain,
            locale: $this->appInfo->lang,
            extraData: [
                ExtraDataProps::BOT_ID => $botId,
                ExtraDataProps::DOMAIN => $this->data->domain,
                ExtraDataProps::APP_CODE => $this->appInfo->code,
            ],
            temporaryData: $this->data->oauthToken->toTemporaryData(),
            paymentType: $this->appInfo->toPaymentType(),
            status: $this->appInfo->toAppExternalStatus(),
            accountType: $this->creds->catalogOnly ? AccountType::CatalogOnly : AccountType::Full,
            subPartnersChain: $this->appInfo->isLocal() ? [ $this->creds->catalogOnly ? 'LOCAL-CATALOG' : 'LOCAL' ] : null,
        );
    }
}