<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\EventHandlers\BotJoinChatHandler;
use BmPlatform\Bitrix24\EventHandlers\BotMessageAddHandler;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event as EventFacade;
use Illuminate\Support\Facades\Log;

class EventHandler
{
    private const E_APP_INSTALL     = 'ONAPPINSTALL';
    private const E_APP_UPDATE      = 'ONAPPUPDATE';
    private const E_BOT_JOIN_CHAT   = 'ONIMBOTJOINCHAT';
    private const E_BOT_MESSAGE_ADD = 'ONIMBOTMESSAGEADD';

    public function __construct(protected Hub $hub)
    {
        //
    }

    /**
     * @param WebhookPayload $payload
     *
     * @return void
     */
    public function __invoke(WebhookPayload $payload): void
    {
        // TODO: implement ONIMBOTDELETE event
        $handlerClass = match ($payload->getEventName()) {
            self::E_BOT_JOIN_CHAT   => BotJoinChatHandler::class,
            self::E_BOT_MESSAGE_ADD => BotMessageAddHandler::class,
            default                 => null,
        };

        if (!($appInstance = $this->getAppInstance($payload))) {
            Log::warning('App not found', [
                'body' => $payload->getRawData(),
                'appType' => 'b24',
            ]);

            return;
        } elseif ($handlerClass === null) {
            Log::warning('Unknown Webhook Event Received', [
                'body' => $payload->getRawData(),
                'appInstance' => $appInstance,
            ]);

            return;
            // Token is not available when installing from ui
        }/* elseif (!$this->checkToken($appInstance, $payload)) {
            Log::warning('Invalid app token', [
                'body' => $payload->getRawData(),
                'appInstance' => $appInstance,
            ]);

            return;
        }*/ else {
            Log::debug('WH', [
                'body' => $payload->getRawData(),
                'appInstance' => $appInstance,
            ]);
        }

        /** @var Event|null $event */
        $event = App::make($handlerClass, compact('payload', 'appInstance'))();

        if ($event !== null) {
            $appInstance->dispatchEvent($event);
        }
    }

    /**
     * @param \BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload $payload
     *
     * @return \BmPlatform\Abstraction\Interfaces\AppInstance|null
     */
    protected function getAppInstance(WebhookPayload $payload): ?AppInstance
    {
        return $this->hub->findAppById('b24', $payload->getAuthPayload()->getMemberId().':'.$payload->getBotPayload()->code);
    }
}
