<?php

declare(strict_types=1);

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\App;

/** @var Router $router */
$router = App::make('router');

$router
    ->name('bitrix24.')
    ->prefix('integrations/bitrix24')
    ->group(function () use ($router) {
        $router->get('/', fn () => response(200));
        $router->get('/install', fn () => response(200));
        $router->post('/', \BmPlatform\Bitrix24\Http\AppController::class);
        $router->post('/install', \BmPlatform\Bitrix24\Http\AppInstallController::class);
        $router->post('webhook', \BmPlatform\Bitrix24\Http\CallbackController::class)->name('callback');
    });
