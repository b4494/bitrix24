<?php

namespace BmPlatform\Bitrix24\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class AppController
{
    public function __invoke(Request $request, Hub $hub, CredentialsRepo $credentialsRepo)
    {
        $data = $this->getPayload($request);

        $appInfo = $data->getApiCommands()->appInfo();

        if (!$credentialsRepo->has($appInfo->code)) {
            return 'This application is not allowed to be used with chatbot builder. '.
                'Please send us your client_id, client_secret and your app code <b>'.$appInfo->code.'</b>';
        }

        if (!$app = $hub->findAppById('b24', $this->getAppExternalId($data, $credentialsRepo->getByCodeOrFail($appInfo->code)))) {
            Log::warning('Failed to resolve app', [
                'body' => $request->input(),
                'appType' => 'b24',
            ]);

            return 'Sorry, your application was not installed. Please reinstall it.';
        }

        Log::debug('Opened app', [
            'body' => $request->input(),
            'appInstance' => $app,
        ]);

        /** @var \BmPlatform\Bitrix24\AppHandler $handler */
        $handler = $app->getHandler();

        $user = $handler->getApiCommands()->currentUser();

        return redirect($hub->webUrl($hub->getAuthToken($app, $user->toOperator())));
    }

    protected function getAppExternalId(AppDisplayPayload $data, Bitrix24AppCredentials $creds): string
    {
//        /** @var \BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Bot $bot */
//        $bot = collect($data->getApiCommands()->getBots())
//            ->firstWhere('code', );

        return $data->memberId.':'.($creds->botCode ?: Config::get('bitrix24.bot.code'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload
     */
    protected function getPayload(Request $request): AppDisplayPayload
    {
        return AppDisplayPayload::fromRequest($request);
    }

}