<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Http;

use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\EventHandler;
use Illuminate\Http\Request;

class CallbackController
{
    public function __invoke(Request $request, EventHandler $eventHandler): mixed
    {
        $eventHandler(new WebhookPayload($request->input()));

        return response('');
    }
}
