<?php

namespace BmPlatform\Bitrix24\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use BmPlatform\Bitrix24\Integrator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppInstallController
{
    public function __invoke(Request $request, CredentialsRepo $credentialsRepo, Hub $hub)
    {
        $data = $this->getPayload($request);

        $appInfo = $data->getApiCommands()->appInfo();

        if (!$credentialsRepo->has($appInfo->code)) {
            return 'This application is not allowed to be used with chatbot builder. '.
                'Please send us your client_id, client_secret and your app code <b>'.$appInfo->code.'</b>';
        }

        $integrator = new Integrator($hub, $credentialsRepo->getByCodeOrFail($appInfo->code), $data, $appInfo);
        $appInstance = $integrator();

        return View::make('b24::install', [
            'redirectUrl' => $hub->webUrl($hub->getAuthToken($appInstance)),
        ]);
    }

    protected function getPayload(Request $request): AppDisplayPayload
    {
        return AppDisplayPayload::fromRequest($request);
    }
}