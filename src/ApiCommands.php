<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Bot;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLine;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChatPaginator;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24ChatUser;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class ApiCommands
{
    /** Do not modify ! */
    public const BOT_CODE = 'bot_marketing_chat_bot';

    private array $cache = [];

    private $metadata;

    public function __construct(
        protected OAuthAccessToken  $token,
        public readonly ApiClient   $client,
        public readonly ?AppHandler $app = null
    ) {
        //
    }

    public function createBot(string $code, string $name, string $email, string $url): int
    {
        return (int)$this->sendRestApiRequest('imbot.register', [
            'CODE' => $code,
            'TYPE' => 'O',
            'EVENT_HANDLER' => App::make('url')->route('bitrix24.callback'),
            'PROPERTIES' => [
                'NAME' => $name,
                'COLOR' => 'LIGHT_BLUE',
                'EMAIL' => $email,
                'PERSONAL_WWW' => $url,
            ],
        ]);
    }

    /**
     * @return Bitrix24Bot[]
     */
    public function getBots(): array
    {
        $bots = [];

        foreach ($this->sendRestApiRequest('imbot.bot.list') as $bot_data) {
            $bots[] = Bitrix24Bot::fromApiResponse($bot_data);
        }

        return $bots;
    }

    public function sendBotMessage(
        int        $bot_id,
        string     $chat_id,
        ?string    $text,
        ?MediaFile $media_file = null,
        bool       $is_system_message = false
    ): int {
        $attach_data = [];

        if ($media_file !== null) {
            $attach_type = $media_file->type === MediaFileType::Image ? 'IMAGE' : 'FILE';

            $attach_data = [
                [
                    $attach_type => [ 'LINK' => $media_file->url ],
                ],
            ];
        }

        return $this->sendRestApiRequest('imbot.message.add', array_filter([
            'BOT_ID' => $bot_id,
            'DIALOG_ID' => "chat$chat_id",
            'MESSAGE' => $text,
            'ATTACH' => $attach_data,
            'SYSTEM' => $is_system_message ? 'Y' : 'N',
        ]));
    }

    public function transferChatToOperator(string $chat_id, string $user_id): void
    {
        $this->sendRestApiRequest('imopenlines.bot.session.transfer', [
            'CHAT_ID' => $chat_id,
            'USER_ID' => $user_id,
            'LEAVE' => 'N',
        ]);
    }

    /**
     * @return Bitrix24User[]
     */
    public function getEmployeeUsers(?int $start = null): array
    {
        $users = [];

        foreach ($this->sendRestApiRequest('user.get', [ 'USER_TYPE' => 'employee', 'start' => $start ?: 0 ]) as $user_data) {
            $users[] = Bitrix24User::fromApiResponse($user_data);
        }

        return $users;
    }

    public function getEmployeeUserById(string $id): ?Bitrix24User
    {
        $response = $this->sendRestApiRequestCached('user.get', [ 'USER_TYPE' => 'employee', 'ID' => $id ]);

        if (empty($response)) {
            return null;
        }

        return Bitrix24User::fromApiResponse(current($response));
    }

    public function updateUser(Bitrix24User $user): void
    {
        $this->sendRestApiRequest('user.update', array_filter([
            'ID' => $user->id,
            'NAME' => $user->firstName,
            'PERSONAL_PHONE' => $user->phone,
        ]));
    }

    public function getChatUserById(int $id): ?Bitrix24ChatUser
    {
        $response = $this->sendRestApiRequest('im.user.get', [
            'ID' => $id,
        ]);

        if (empty($response)) {
            return null;
        }

        return Bitrix24ChatUser::fromApiResponse($response);
    }

    /**
     * @param int $chat_id
     *
     * @return int[]
     */
    public function getChatUserIds(int $chat_id): array
    {
        return $this->sendRestApiRequestCached('im.chat.user.list', [
            'CHAT_ID' => $chat_id,
        ]);
    }

    /**
     * @return string[]
     */
    public function getOpenLineIds(): array
    {
        $response = $this->sendRestApiRequest('imopenlines.config.list.get');

        return Arr::pluck($response, 'ID');
    }

    /** @return Bitrix24OpenLine[] */
    public function getActiveOpenLines(?array $select = null): array
    {
        return array_map(fn ($v) => new Bitrix24OpenLine($v), $this->sendRestApiRequest('imopenlines.config.list.get', [
            'PARAMS' => array_filter([
                'select' => $select,
                'filter' => [ 'ACTIVE' => 'Y' ],
            ]),
        ]));
    }

    public function isOpenLineConnectorConnected(string $open_line_id, string $connector_id): bool
    {
        $response = $this->sendRestApiRequest('imconnector.status', [
            'LINE' => $open_line_id,
            'CONNECTOR' => $connector_id,
        ]);

        return Arr::get($response, 'STATUS') === true;
    }

    public function setOpenLineBot(string $open_line_id, int $bot_id): void
    {
        $this->sendRestApiRequest('imopenlines.config.update', [
            'CONFIG_ID' => $open_line_id,
            'PARAMS' => [
                'WELCOME_BOT_ENABLE' => 'Y',
                'WELCOME_BOT_JOIN' => 'always',
                'WELCOME_BOT_ID' => $bot_id,
            ],
        ]);
    }

    public function getContactByChatUserId(string $user_id): ?Bitrix24Contact
    {
        $response = $this->sendRestApiRequestCached('crm.contact.list', [
            'filter' => [ 'CREATED_BY_ID' => $user_id ],
            'select' => [ 'ID', 'NAME', 'EMAIL', 'WEB', 'PHONE', 'PHOTO', 'COMMENTS' ],
        ]);

        if (empty($response)) {
            return null;
        }

        return Bitrix24Contact::fromApiResponse(current($response));
    }

    public function getChatOperatorById(int $chat_id, ?int $bot_id = null, ?string $contact_id = null): ?Bitrix24User
    {
        $chat_user_ids = $this->getChatUserIds($chat_id);

        foreach (array_diff($chat_user_ids, [ $bot_id, (int)$contact_id ]) as $id) {
            $operator = $this->getEmployeeUserById((string)$id);

            if ($operator !== null) {
                return $operator;
            }
        }

        return null;
    }

    public function updateContact(Bitrix24Contact $contact): void
    {
        $this->sendRestApiRequest('crm.contact.update', array_filter([
            'ID' => $contact->id,
            'FIELDS' => [
                'NAME' => $contact->name,
                'COMMENTS' => $contact->comments,
                'PHONE' => array_map(fn(string $phone) => [ 'VALUE' => $phone ], $contact->phones),
                'EMAIL' => array_map(fn(string $email) => [ 'VALUE' => $email ], $contact->emails),
            ],
        ]));
    }

    public function getRecentOpenLineChats(?string $last_message_data = null): Bitrix24OpenLineChatPaginator
    {
        $response = $this->sendRestApiRequest('im.recent.list', array_filter([
            'LAST_MESSAGE_DATE' => $last_message_data,
            'SKIP_DIALOG' => 'Y',
            'SKIP_CHAT' => 'Y',
            'SKIP_OPENLINES' => 'N',
        ]));

        return new Bitrix24OpenLineChatPaginator(
            array_map(fn(array $item) => Bitrix24OpenLineChat::fromApiResponse($item), Arr::get($response, 'items')),
            Arr::get($response, 'hasMore')
        );
    }

    public function appInfo(): Bitrix24AppInfo
    {
        return Bitrix24AppInfo::fromApiResponse($this->sendRestApiRequest('app.info'));
    }

    public function currentUser(): Bitrix24User
    {
        return Bitrix24User::fromApiResponse($this->sendRestApiRequest('user.current'));
    }

    private function sendRestApiRequestCached(string $action, array $query = []): mixed
    {
        $key = $action.http_build_query($query);

        if (!isset($this->cache[$key])) {
            $this->cache[$key] = $this->sendRestApiRequest($action, $query);
        }

        return $this->cache[$key];
    }

    public function sendRestApiRequest(string $action, array $query = []): mixed
    {
        try {
            $response = $this->client->sendRestApiRequest($this->token, $action, $query);

            $this->metadata = Arr::except($response, 'result');

            return $response['result'] ?? null;
        }

        catch (ErrorException $e) {
            if ($e->errorCode === ErrorCode::AuthenticationFailed && isset($this->app)) {
                $this->token = $this->app->getOAuthAccessToken(forceRefresh: true);

                return $this->client->sendRestApiRequest($this->token, $action, $query)['result'] ?? null;
            }

            throw $e;
        }
    }

    /** @param array<int|string, array{method: string, query: array}> $requests */
    public function batchRequest(array $requests): array
    {
        return $this->sendRestApiRequest('batch', [
            'cmd' => array_map(fn (array $request) => $request['method'].'?'.http_build_query($request['query']), $requests),
        ])['result'];
    }

    public function getMetadata(): mixed
    {
        return $this->metadata;
    }

    public function getCursor(): ?int
    {
        $next = $this->metadata['next'] ?? null;
        $total = $this->metadata['total'] ?? null;

        return $next && $total && $next < $total ? $next : null;
    }

    public function checkMethodExists(string $name): bool
    {
        $resp = $this->sendRestApiRequestCached('method.get', compact('name'));

        return $resp['isAvailable'] && $resp['isExisting'];
    }

    /** @return array<string, string> */
    public function getConnectorsList(): array
    {
        return $this->sendRestApiRequest('imconnector.list');
    }
}
