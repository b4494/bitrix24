<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\EventHandlers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Entities\Webhook\AuthPayload;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use Illuminate\Config\Repository;

abstract class AbstractEventHandler
{
    public function __construct(
        protected AppInstance $appInstance,
        protected WebhookPayload $payload,
    ) {
        //
    }

    abstract public function __invoke(): mixed;

}
