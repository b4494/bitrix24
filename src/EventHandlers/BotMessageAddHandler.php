<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\EventHandlers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Bitrix24\Presenters\ChatPresenter;
use BmPlatform\Bitrix24\Presenters\ContactPresenter;
use BmPlatform\Bitrix24\Presenters\MessageDataPresenter;
use BmPlatform\Bitrix24\Presenters\OperatorPresenter;

class BotMessageAddHandler extends AbstractEventHandler
{
    public function __invoke(): InboxReceived|OutboxSent|null
    {
        // Check if chat is open line chat
        if ($this->payload->getParamsPayload()->getChatType() != 'L') return null;

        if ($this->payload->getUserPayload()->isExtranet()) {
            return $this->makeInboxReceived();
        } elseif (!$this->payload->getParamsPayload()->isSystem()) {
            return $this->makeOutboxSent();
        } else {
            return null;
        }
    }

    public function makeInboxReceived(): InboxReceived
    {
        return new InboxReceived(
            chat: $this->payload->getParamsPayload()->getOpenLineChat()->toChat(),
            participant: $this->payload->getUserPayload()->toContact(),
            message: MessageDataPresenter::make(
                data: $this->payload->getParamsPayload()->getMessage(
                    $this->payload->getBotPayload()->auth->getApiCommands($this->appInstance),
                ),
            ),
            timestamp: $this->payload->getTimestamp()
        );
    }

    public function makeOutboxSent(): OutboxSent
    {
        return new OutboxSent(
            chat: $this->payload->getParamsPayload()->getOpenLineChat()->toChat(),
            message: MessageDataPresenter::make(
                data: $this->payload->getParamsPayload()->getMessage(
                    $this->payload->getBotPayload()->auth->getApiCommands($this->appInstance),
                ),
            ),
            operator: $this->payload->getUserPayload()->toOperator(),
            timestamp: $this->payload->getTimestamp()
        );
    }
}
