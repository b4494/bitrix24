<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\EventHandlers;

use BmPlatform\Abstraction\Events\CustomEventOccurred;

class BotJoinChatHandler extends AbstractEventHandler
{
    public function __invoke(): ?CustomEventOccurred
    {
        if (!$chatData = $this->payload->getParamsPayload()->getOpenLineChat()) return null;

        return new CustomEventOccurred(
            chat: $chatData->toChat(),
            eventId: 'botJoin',
            timestamp: $this->payload->getTimestamp(),
        );
    }
}
