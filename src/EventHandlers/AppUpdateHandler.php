<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\EventHandlers;

use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;

class AppUpdateHandler extends AbstractEventHandler
{
    public function __invoke(): mixed
    {
        /** @var AppHandler $app_handler */
        $app_handler = $this->appInstance->getHandler();

        $app_handler->freshTemporaryData($this->payload->getAuthPayload()->getOAuthAccessToken()->toTemporaryData());

        return null;
    }
}
