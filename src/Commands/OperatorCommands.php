<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Bitrix24\Presenters\OperatorPresenter;

trait OperatorCommands
{
    protected ApiCommands $api;
    protected string $domain;

    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        if ($request->target instanceof OperatorGroup) {
            throw new ErrorException(
                ErrorCode::FeatureNotSupported,
                'Bitrix24 doesn\'t have a representation of OperatorGroup'
            );
        }

        $bitrix_user = $this->api->getEmployeeUserById($request->target->getExternalId());

        if ($bitrix_user === null) {
            throw new ErrorException(ErrorCode::OperatorNotFound);
        }

        $this->api->transferChatToOperator($request->chat->getExternalId(), $request->target->getExternalId());

        return new NewOperatorResponse(OperatorPresenter::make(
            operator: $bitrix_user,
            domain: $this->domain
        ));
    }
}
