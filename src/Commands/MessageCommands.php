<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Commands;

use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Bitrix24\ApiCommands;

trait MessageCommands
{
    protected ApiCommands $api;
    protected int $bot_id;

    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        $this->api->sendBotMessage(
            $this->bot_id,
            $request->chat->getExternalId(),
            $request->text,
            is_system_message: true,
        );
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return new MessageSendResult(
            externalId: (string) $this->api->sendBotMessage(
                $this->bot_id,
                $request->chat->getExternalId(),
                $request->text,
            )
        );
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        return new MessageSendResult(
            externalId: (string) $this->api->sendBotMessage(
                $this->bot_id,
                $request->chat->getExternalId(),
                $request->caption,
                $request->file
            )
        );
    }
}
