<?php

namespace BmPlatform\Bitrix24;

class ExtraDataProps
{
    const APP_CODE = 'a';
    const BOT_ID = 'b';
    const DOMAIN = 'd';

    const COMPANY_ID = 'cid';
    const CONTACT_ID = 'cnid';
    const DEAL_ID = 'did';
    const LEAD_ID = 'lid';
    const FIRST_NAME = 'fn';
    const LAST_NAME = 'ln';
}