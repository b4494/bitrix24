<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\OAuth;

use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Bitrix24\ApiClient;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class OAuthAccessToken
{
    public function __construct(
        public readonly string $accessToken,
        public readonly int    $expires,
        public readonly string $clientEndpoint,
        public readonly string $refreshToken
    ) {
    }

    public static function fromApiResponse(array $response): self
    {
        return new OAuthAccessToken(
            accessToken: $response['access_token'] ?? null,
            expires: (int) ($response['expires'] ?? null),
            clientEndpoint: $response['client_endpoint'] ?? null,
            refreshToken: $response['refresh_token'] ?? null,
        );
    }

    public static function fromTemporaryData(TemporaryData $temporaryData): self
    {
        $data = explode(';', $temporaryData->data);

        return new self($data[0], $temporaryData->expiresAt->timestamp, $data[1], $data[2]);
    }

    public function toTemporaryData(): TemporaryData
    {
        return new TemporaryData(
            sprintf("%s;%s;%s", $this->accessToken, $this->clientEndpoint, $this->refreshToken),
            Carbon::createFromTimestamp($this->expires),
        );
    }

    public function refresh(ApiClient $client, Bitrix24AppCredentials $creds): self
    {
        return OAuthAccessToken::fromApiResponse(
            $client->sendOAuthRequest([
                'grant_type' => 'refresh_token',
                'client_id' => $creds->clientId,
                'client_secret' => $creds->clientSecret,
                'refresh_token' => $this->refreshToken,
            ])
        );
    }
}
