<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\OAuth;

class Bitrix24AppCredentials
{
    public function __construct(
        public readonly string $id,
        public readonly string $clientId,
        public readonly string $clientSecret,
        public readonly ?string $botCode = null,
        public readonly ?string $botName = null,
        public readonly bool $catalogOnly = false,
    ) {
        //
    }

    public static function fromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['client_id'],
            $data['client_secret'],
            $data['bot_code'] ?? null,
            $data['bot_name'] ?? null,
            $data['catalog_only'] ?? false,
        );
    }

    public function toArray(): array
    {
        return [ 'id' => $this->id, 'client_id' => $this->clientId, 'client_secret' => $this->clientSecret ];
    }

    public function isLocal(): bool
    {
        return str($this->clientId)->startsWith('local');
    }
}
