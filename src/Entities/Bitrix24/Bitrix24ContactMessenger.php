<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;

class Bitrix24ContactMessenger
{
    public function __construct(
        public ExternalMessengerTypeEnum $type,
        public string $handle
    ) {
    }
}
