<?php

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

/**
 * @property string $ID
 * @property string $LINE_NAME
 */
class Bitrix24OpenLine
{
    public function __construct(protected readonly array $data)
    {
        //
    }

    public function __get(string $name)
    {
        return $this->data[$name] ?? null;
    }
}