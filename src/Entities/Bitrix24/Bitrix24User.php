<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Bitrix24\ExtraDataProps;
use Illuminate\Support\Arr;

class Bitrix24User
{
    public function __construct(
        public string  $id,
        public ?string $firstName = null,
        public ?string $lastName = null,
        public ?string $photo = null,
        public ?string $phone = null,
        public ?string $email = null
    ) {
        //
    }

    public static function fromApiResponse(array $response): self
    {
        return new Bitrix24User(
            id: $response['ID'] ?? null,
            firstName: $response['NAME'] ?? null,
            lastName: $response['LAST_NAME'] ?? null,
            photo: $response['PERSONAL_PHOTO'] ?? null,
            phone: $response['PERSONAL_MOBILE'] ?? null,
            email: $response['EMAIL'] ?? null,
        );
    }

    public function toOperator(): Operator
    {
        return new Operator(
            externalId: $this->id,
            name: implode(' ', array_filter([ $this->firstName, $this->lastName ])),
            phone: $this->phone,
            email: $this->email,
            avatarUrl: $this->photo,
            extraData: [
                ExtraDataProps::FIRST_NAME => $this->firstName,
                ExtraDataProps::LAST_NAME => $this->lastName,
            ]
        );
    }
}
