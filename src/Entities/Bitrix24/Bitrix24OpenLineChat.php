<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Bitrix24\ExtraDataProps;
use BmPlatform\Bitrix24\Presenters\MessengerInstancePresenter;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Bitrix24OpenLineChat
{
    public function __construct(
        public int    $id,
        public int $openLineId,
        public string $messengerType,
        public string $externalUserId,
        public int $startUserId,
        public int $authorId,
        /** @var array{LEAD: int, DEAL: int, CONTACT: int, COMPANY: int} */
        public ?array $entityRefs,
        public ?int   $userCounter = null,
    ) {
    }

    public function toChat(): Chat
    {
        return new Chat(
            externalId: (string)$this->id,
            messengerInstance: MessengerInstancePresenter::externalId($this->messengerType, $this->openLineId),
            contact: $this->startUserId ? (string)$this->startUserId : null,
            operator: $this->authorId ? (string)$this->authorId : null,
            messengerId: $this->externalUserId,
            extraData: isset($this->entityRefs) ? [
                ExtraDataProps::DEAL_ID => $this->entityRefs['DEAL'] ?? null,
                ExtraDataProps::LEAD_ID => $this->entityRefs['LEAD'] ?? null,
                ExtraDataProps::CONTACT_ID => $this->entityRefs['CONTACT'] ?? null,
                ExtraDataProps::COMPANY_ID => $this->entityRefs['COMPANY'] ?? null,
            ] : null,
        );
    }

    public static function make(int $id, ?int $authorId, string $entityId, ?string $entityData2, ?int $userCounter = null): self
    {
        $entityParts = self::parseChatEntityId($entityId);

        // TODO: extract dialog id from entity_data_1
        return new self(
            id: $id,
            openLineId: (int)$entityParts['open_line_id'],
            messengerType: $entityParts['messenger_external_type'],
            externalUserId: $entityParts['external_user_id'],
            startUserId: (int)$entityParts['user_id'],
            authorId: (int)$authorId,
            entityRefs: $entityData2 ? self::parseEntityData($entityData2) : null,
            userCounter: $userCounter,
        );
    }

    public static function fromApiResponse(array $response): self
    {
        return self::make((int)$response['chat_id'], null, $response['chat']['entity_id'], $response['chat']['entity_data_2'] ?? null, $response['chat']['user_counter']);
    }

    public static function fromWebhookParamsPayload(array $response): ?self
    {
        return self::make((int)$response['CHAT_ID'], (int)$response['CHAT_AUTHOR_ID'], $response['CHAT_ENTITY_ID'], $response['CHAT_ENTITY_DATA_2'] ?? null);
    }

    private static function parseChatEntityId(?string $chat_entity_id): array
    {
        if (Str::substrCount($chat_entity_id ?? '', '|') === 3) {
            return array_combine(['messenger_external_type', 'open_line_id', 'external_user_id', 'user_id'], explode('|', $chat_entity_id ?? ''));
        }

        return [];
    }

    public static function parseEntityData(string $entityData2): array
    {
        $parts = explode('|', $entityData2);
        $result = [];
        $total = count($parts);

        for ($i = 0; $i < $total; $i += 2) {
            $result[$parts[$i]] = (int)$parts[$i+1];
        }

        return $result;
    }
}
