<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Bitrix24\ApiCommands;
use Illuminate\Support\Arr;

class Bitrix24Message
{
    /**
     * @param string $id
     * @param string|null $message
     * @param Bitrix24MessageFile[] $files
     */
    public function __construct(
        public string  $id,
        public ?string $message,
        public array   $files
    ) {
    }

    public static function fromWebhookParamsPayload(array $payload, ?ApiCommands $apiCommands): self
    {
        return new self(
            id: $payload['MESSAGE_ID'],
            message: $payload['MESSAGE'] ?? null,
            files: ($apiCommands ? static::getFilesFromPayload($payload, $apiCommands) : null) ?: [],
        );
    }

    public static function getFilesFromPayload(array $payload, ApiCommands $apiCommands): ?array
    {
        if (!isset($payload['FILES'])) return null;

        $requests = ($files = collect($payload['FILES'])->values())->mapWithKeys(static fn ($file) => [
            $file['id'] => [ 'method' => 'disk.file.get', 'query' => [ 'id' => $file['id'] ] ],
        ])->all();

        $responses = $apiCommands->batchRequest($requests);

        return $files->map(fn ($file) => new Bitrix24MessageFile(
            type: $file['type'],
            url: $responses[$file['id']]['DOWNLOAD_URL'],
            extension: $file['extension'],
            name: $file['name'],
        ))->all();
    }
}
