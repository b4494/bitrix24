<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

class Bitrix24MessageFile
{
    public function __construct(
        public string $type,
        public string $url,
        public string $extension,
        public string $name,
    ) {
        //
    }
}
