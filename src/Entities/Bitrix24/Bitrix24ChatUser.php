<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use Illuminate\Support\Arr;

class Bitrix24ChatUser
{
    public function __construct(
        public int $id,
        public string $name,
        public string $avatar,
        public ?string $personal_phone = null
    ) {
    }

    public static function fromApiResponse(array $response): self
    {
        return new Bitrix24ChatUser(
            Arr::get($response, 'id'),
            Arr::get($response, 'name'),
            Arr::get($response, 'avatar'),
            Arr::get($response, 'phones.personal_phone')
        );
    }
}
