<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class Bitrix24Contact
{
    /**
     * @param string $id
     * @param string|null $name
     * @param array $emails
     * @param array $phones
     * @param string|null $photo_url
     * @param string|null $comments
     * @param Bitrix24ContactMessenger[] $messengers
     */
    public function __construct(
        public string  $id,
        public ?string $name = null,
        public array   $emails = [],
        public array   $phones = [],
        public ?string $photo_url = null,
        public ?string  $comments = null,
        public array   $messengers = []
    ) {
    }

    public static function fromApiResponse(array $response): self
    {
        $emails = array_map(fn (array $item): string => Arr::get($item, 'VALUE'), Arr::get($response, 'EMAIL', []));
        $phones = array_map(fn (array $item): string => Arr::get($item, 'VALUE'), Arr::get($response, 'PHONE', []));

        $messengers = [];

        $url_type_map = [
            'https://t.me/'              => [ExternalMessengerTypeEnum::TELEGRAM_BOT],
            'https://wa.me/'             => [ExternalMessengerTypeEnum::WHATSAPP_BY_TWILIO, ExternalMessengerTypeEnum::WHATSAPP_BY_ENDA],
            'imessage://'                => [ExternalMessengerTypeEnum::APPLE_MESSAGE_FOR_BUSINNESS],
            'https://vk.com/'            => [ExternalMessengerTypeEnum::VK],
            'https://ok.ru/profile/'     => [ExternalMessengerTypeEnum::OK],
            'https://facebook.com/'      => [ExternalMessengerTypeEnum::FACEBOOK_MESSAGES],
            'https://www.instagram.com/' => [ExternalMessengerTypeEnum::INSTAGRAM_DIRECT],
            'https://instagram.com/'     => [ExternalMessengerTypeEnum::INSTAGRAM_DIRECT],
        ];

        foreach (Arr::get($response, 'WEB', []) as $web_data) {
            foreach ($url_type_map as $url => $types) {
                $messenger_url = Arr::get($web_data, 'VALUE');

                if (Str::startsWith($messenger_url, $url)) {
                    foreach ($types as $type) {
                        $messengers[] = new Bitrix24ContactMessenger(
                            $type,
                            Str::replace($url, '', $messenger_url)
                        );
                    }
                }
            }
        }

        return new self(
            id: Arr::get($response, 'ID'),
            name: Arr::get($response, 'NAME'),
            emails: $emails,
            phones: $phones,
            photo_url: Arr::get($response, 'PHOTO.showUrl'),
            comments: Arr::get($response, 'COMMENTS'),
            messengers: $messengers
        );
    }

    public function getFirstPhone(): ?string
    {
        return count($this->phones) > 0 ? current($this->phones) : null;
    }

    public function getFirstEmail(): ?string
    {
        return count($this->emails) > 0 ? current($this->emails) : null;
    }
}
