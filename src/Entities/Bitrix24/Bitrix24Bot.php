<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use Illuminate\Support\Arr;

class Bitrix24Bot
{
    public function __construct(
        public int $id,
        public string $code,
        public string $name
    ) {
    }

    public static function fromApiResponse(array $response): self
    {
        return new Bitrix24Bot(
            id: Arr::get($response, 'ID'),
            code: Arr::get($response, 'CODE'),
            name: Arr::get($response, 'NAME'),
        );
    }
}
