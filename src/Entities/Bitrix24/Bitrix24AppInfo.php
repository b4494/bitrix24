<?php

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\Exceptions\ErrorException;

class Bitrix24AppInfo
{
    public function __construct(
        public readonly string $id,
        public readonly string $code,
        public readonly string $version,
        public readonly string $status,
        public readonly bool $paymentExpired,
        public readonly ?int $days,
        public readonly bool $installed,
        public readonly string $lang,
    ) {
    }

    public static function fromApiResponse(array $data): self
    {
        return new self(
            $data['ID'],
            $data['CODE'],
            $data['VERSION'],
            $data['STATUS'],
            $data['PAYMENT_EXPIRED'] == 'Y',
            $data['DAYS'],
            $data['INSTALLED'] == 'Y',
            $data['LANGUAGE_ID'],
        );
    }

    public static function fromAppInstallData(WebhookPayload $payload): self
    {
        $authData = $payload->getAuthPayload();
        $data = $payload->getData();

        return new self('unknown', 'unknown',
            version: $data['VERSION'],
            status: $authData->getStatus(),
            paymentExpired: false,
            days: ($date = $authData->getDateFinish()) ? $date->diffInDays() : null,
            installed: $data['INSTALLED'] === 'Y',
            lang: $data['LANGUAGE_ID'],
        );
    }
    public function toPaymentType(): PaymentType
    {
        return PaymentType::Internal;
//        return $this->status === 'F' || $this->status === 'L' ? PaymentType::Internal : PaymentType::External;
    }

    public function toAppExternalStatus(): AppExternalStatus
    {
        if ($this->status === 'F' || $this->status === 'L') {
            return new AppExternalStatus(AppStatus::Active);
        } elseif ($this->paymentExpired) {
            return new AppExternalStatus(AppStatus::Frozen);
        }

        return new AppExternalStatus(
            AppStatus::Active,

            match ($this->status) {
                'T' => PaymentStatus::Trial,
                'S', 'P' => PaymentStatus::Paid,
                default => throw new ErrorException(ErrorCode::UnexpectedData, 'Unknown app status ['.$this->status.']'),
            },

            expiresAt: now()->endOfDay()->addDays($this->days),
        );
    }

    public function isLocal(): bool
    {
        return $this->status == 'L';
    }
}