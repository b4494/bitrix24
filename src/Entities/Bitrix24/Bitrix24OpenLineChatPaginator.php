<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Bitrix24;

use Illuminate\Support\Arr;

class Bitrix24OpenLineChatPaginator
{
    /**
     * @param Bitrix24OpenLineChat[] $items
     * @param bool $has_more
     */
    public function __construct(
        public array $items,
        public bool $has_more
    ) {
    }

    public function getCursor(): ?string
    {
        return $this->has_more ? Arr::last($this->items)?->last_message_date?->toDateString() : null;
    }
}
