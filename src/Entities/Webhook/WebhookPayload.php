<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Webhook;

use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class WebhookPayload
{
    private ?AuthPayload $authPayload = null;
    private ?WebhookParamsPayload $paramsPayload = null;
    private ?WebhookUserPayload $userPayload = null;
    private ?BotPayload $botPayload = null;

    public function __construct(private readonly array $data)
    {
        //
    }

    public function getEventName(): string
    {
        return $this->data['event'];
    }

    public function getAuthPayload(): AuthPayload
    {
        if ($this->authPayload === null) {
            $this->authPayload = new AuthPayload($this->data['auth']);
        }

        return $this->authPayload;
    }

    public function getParamsPayload(): WebhookParamsPayload
    {
        if ($this->paramsPayload === null) {
            $this->paramsPayload = new WebhookParamsPayload($this->data['data']['PARAMS']);
        }

        return $this->paramsPayload;
    }

    public function getUserPayload(): WebhookUserPayload
    {
        if ($this->userPayload === null) {
            $this->userPayload = new WebhookUserPayload($this->data['data']['USER']);
        }

        return $this->userPayload;
    }

    public function getTimestamp(): Carbon
    {
        return Carbon::createFromTimestamp($this->data['ts']);
    }

    public function getData(): array
    {
        return $this->data['data'];
    }

    public function getRawData(): array
    {
        return $this->data;
    }

    public function getApiCommands(): ApiCommands
    {
        return new ApiCommands($this->getAuthPayload()->getOAuthAccessToken(), new ApiClient());
    }

    public function getBotPayload(): BotPayload
    {
        if ($this->botPayload) return $this->botPayload;

        return $this->botPayload = BotPayload::fromPayload(array_values($this->data['data']['BOT'])[0]);
    }
}
