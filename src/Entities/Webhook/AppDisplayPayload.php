<?php

namespace BmPlatform\Bitrix24\Entities\Webhook;

use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use Illuminate\Http\Request;

class AppDisplayPayload
{
    public function __construct(
        public readonly OAuthAccessToken $oauthToken,
        public readonly string $memberId,
        public readonly string $domain,
        public readonly string $appToken,
        public readonly string $status,
        public readonly string $lang,
        public readonly string $placement,
        public readonly ?array $placementOptions,
    ) {
        //
    }

    public static function fromRequest(Request $request): static
    {
        $data = $request->input();

        return new self(
            new OAuthAccessToken($data['AUTH_ID'], time() + $data['AUTH_EXPIRES'], 'https://'.$data['DOMAIN'].'/rest/', $data['REFRESH_ID']),
            $data['member_id'],
            $data['DOMAIN'],
            '', // App token is not available
            $data['status'],
            $data['LANG'],
            $data['PLACEMENT'],
            isset($data['PLACEMENT_OPTIONS']) ? json_decode($data['PLACEMENT_OPTIONS'], true, JSON_THROW_ON_ERROR) : null,
            $data['catalogOnly'] ?? false,
        );
    }

    public function isLocal(): bool
    {
        return $this->status === 'L';
    }

    public function getApiCommands(): ApiCommands
    {
        return new ApiCommands($this->oauthToken, new ApiClient());
    }
}