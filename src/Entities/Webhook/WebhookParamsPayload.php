<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Webhook;

use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Message;

class WebhookParamsPayload
{
    public function __construct(private readonly array $data)
    {
        //
    }

    public function isSystem(): bool
    {
        return ($this->data['SYSTEM'] ?? 'N') === 'Y';
    }

    public function getChatType(): ?string
    {
        return $this->data['CHAT_TYPE'] ?? null;
    }

    public function getOpenLineChat(): ?Bitrix24OpenLineChat
    {
        return Bitrix24OpenLineChat::fromWebhookParamsPayload($this->data);
    }

    public function getMessage(?ApiCommands $apiCommands): Bitrix24Message
    {
        return Bitrix24Message::fromWebhookParamsPayload($this->data, $apiCommands);
    }
}
