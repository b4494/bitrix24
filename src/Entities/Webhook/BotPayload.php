<?php

namespace BmPlatform\Bitrix24\Entities\Webhook;

class BotPayload
{
    public function __construct(public readonly int $id, public readonly string $code, public readonly AuthPayload $auth)
    {
        //
    }

    public static function fromPayload(array $data): self
    {
        return new static((int)$data['BOT_ID'], $data['BOT_CODE'], new AuthPayload($data['AUTH']));
    }
}