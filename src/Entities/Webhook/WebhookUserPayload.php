<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Webhook;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Bitrix24\ExtraDataProps;
use Illuminate\Support\Arr;

class WebhookUserPayload
{
    public function __construct(private readonly array $data)
    {
        //
    }

    public function getId(): string
    {
        return $this->data['ID'];
    }

    public function isExtranet(): bool
    {
        return $this->data['IS_EXTRANET'] === 'Y';
    }

    public function toContact(): Contact
    {
        return new Contact(
            externalId: $this->getId(),
            name: $this->getName(),
            extraData: [
                ...(isset($this->data['FIRST_NAME']) ? [ExtraDataProps::FIRST_NAME=>$this->data['FIRST_NAME']] : []),
                ...(isset($this->data['LAST_NAME']) ? [ExtraDataProps::LAST_NAME=>$this->data['LAST_NAME']] : []),
            ]
        );
    }

    public function toOperator(): Operator
    {
        return new Operator(
            externalId: $this->getId(),
            name: $this->getName(),
            extraData: [
                ...(isset($this->data['FIRST_NAME']) ? [ExtraDataProps::FIRST_NAME=>$this->data['FIRST_NAME']] : []),
                ...(isset($this->data['LAST_NAME']) ? [ExtraDataProps::LAST_NAME=>$this->data['LAST_NAME']] : []),
            ]
        );
    }

    /**
     * @return mixed
     */
    protected function getName(): mixed
    {
        return $this->data['NAME'] ?? null;
    }
}
