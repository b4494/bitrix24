<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Entities\Webhook;

use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class AuthPayload
{
    public function __construct(private readonly array $data)
    {
        //
    }

    public function getMemberId(): string
    {
        return $this->data['member_id'];
    }

    public function getDomain(): string
    {
        return $this->data['domain'];
    }

    public function getApplicationToken(): string
    {
        return $this->data['application_token'];
    }

    public function getStatus(): string
    {
        return $this->data['status'];
    }

    public function getDateFinish(): ?Carbon
    {
        return isset($this->data['date_finish']) ? Carbon::createFromTimestamp($this->data['date_finish']) : null;
    }

    public function getOAuthAccessToken(): ?OAuthAccessToken
    {
        if (!isset($this->data['access_token'])) return null;

        return OAuthAccessToken::fromApiResponse($this->data);
    }

    public function getApiCommands(AppInstance $appInstance): ?ApiCommands
    {
        if (!$token = $this->getOAuthAccessToken()) return null;

        return new ApiCommands($token, new ApiClient([ 'appInstance' => $appInstance ]));
    }
}
