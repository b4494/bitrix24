<?php

namespace BmPlatform\Bitrix24\Enums;

enum ExternalMessengerTypeEnum: string
{
    case WHATSAPP_BY_TWILIO = 'whatsappbytwilio';
    case VIBER = 'viber';
    case TELEGRAM_BOT = 'telegrambot';
    case APPLE_MESSAGE_FOR_BUSINNESS = 'imessage';
    case VK = 'vkgroup';
    case OK = 'ok';
    case FACEBOOK_MESSAGES = 'facebook';
    case FACEBOOK_COMMENTS = 'facebook_comments';
    case INSTAGRAM_DIRECT = 'fbinstagramdirect';
    case WHATSAPP_BY_ENDA = 'whatsappbyedna';
    case LIVE_CHAT = 'livechat';
}
