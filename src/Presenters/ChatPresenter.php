<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Presenters;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use Illuminate\Support\Arr;

class ChatPresenter
{
    public static function make(Bitrix24OpenLineChat $b24Chat, ?Bitrix24Contact $contact, ?Bitrix24User $operator, ?string $domain, array $config_messenger_types): Chat
    {
        return new Chat(
            externalId: (string) $b24Chat->id,
            messengerInstance: MessengerInstancePresenter::externalId($b24Chat->messengerType, $b24Chat->openLineId),
            contact: (string)$b24Chat->startUserId,
            operator: $operator === null || $domain === null ? null : OperatorPresenter::make($operator, $domain),
        );
    }

    public static function makeWithContactOnly(Bitrix24OpenLineChat $bitrix_24_chat, ?Bitrix24Contact $contact, array $config_messenger_types): Chat
    {
        return self::make(
            b24Chat: $bitrix_24_chat,
            contact: $contact,
            operator: null,
            domain: null,
            config_messenger_types: $config_messenger_types
        );
    }
}
