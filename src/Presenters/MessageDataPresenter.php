<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Presenters;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Message;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24MessageFile;
use Illuminate\Support\Arr;
use Symfony\Component\Mime\MimeTypes;

class MessageDataPresenter
{
    public static function make(Bitrix24Message $data): MessageData
    {
        return new MessageData(
            externalId: $data->id,
            text: $data->message,
            attachments: array_map(fn (Bitrix24MessageFile $file) => new MediaFile(
                type: self::mapFileType($file),
                url: $file->url,
                mime: Arr::first(MimeTypes::getDefault()->getMimeTypes(pathinfo($file->name, PATHINFO_EXTENSION))),
                name: $file->name,
            ), $data->files)
        );
    }

    private static function mapFileType(Bitrix24MessageFile $file): MediaFileType
    {
        $type_map = [
            'video' => MediaFileType::Video,
            'image' => MediaFileType::Image,
            'audio' => MediaFileType::Audio
        ];

        if (isset($type_map[$file->type])) {
            return $type_map[$file->type];
        }

        if ($file->extension === 'oga') {
            return MediaFileType::Voice;
        }

        return MediaFileType::Document;
    }
}
