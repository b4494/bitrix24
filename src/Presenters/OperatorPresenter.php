<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Presenters;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;

class OperatorPresenter
{
    public static function make(Bitrix24User $operator, string $domain): Operator
    {
        return new Operator(
            externalId: $operator->id,
            name: $operator->firstName ?? '',
            phone: (string) $operator->phone === '' ? null : $operator->phone,
            email: (string) $operator->email === '' ? null : $operator->email,
            avatarUrl: (string) $operator->photo === '' ? null : ($domain . $operator->photo)
        );
    }
}
