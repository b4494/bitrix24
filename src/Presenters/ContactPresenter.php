<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Presenters;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24ChatUser;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;

class ContactPresenter
{
    public static function make(Bitrix24ChatUser $bitrix_24_chat_user, ?Bitrix24Contact $bitrix_24_contact, Chat $chat, string $domain): Contact
    {
        $avatar_url = $bitrix_24_chat_user->avatar;

        if ($avatar_url === '') {
            $avatar_url = $bitrix_24_contact?->photo_url === null ? null : ('https://' . $domain . $bitrix_24_contact->photo_url);
        }

        return new Contact(
            externalId: (string) $bitrix_24_chat_user->id,
            name: $bitrix_24_chat_user->name,
            phone: $bitrix_24_contact?->getFirstPhone(),
            email: $bitrix_24_contact?->getFirstEmail(),
            avatarUrl: $avatar_url,
            extraFields: [
                'phones' => $bitrix_24_contact?->phones,
                'emails' => $bitrix_24_contact?->emails
            ],
            messengerId: $chat->messengerId
        );
    }
}
