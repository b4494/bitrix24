<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Presenters;

use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLine;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;

class MessengerInstancePresenter
{
    public static function make(string $id, string $type, Bitrix24OpenLine $openLine): MessengerInstance
    {
        $schema = Config::get('bitrix24.messenger_types.'.$type);

        return new MessengerInstance(
            externalType: $type,
            externalId: static::externalId($id, (int)$openLine->ID),
            name: $openLine->LINE_NAME,
            internalType: MessengerType::tryFrom($schema['internal_type'] ?? ''),
        );
    }

    public static function externalId(string $type, int $openLineId): string
    {
        return "$openLineId|$type";
    }
}
