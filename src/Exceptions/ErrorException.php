<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;

class ErrorException extends \BmPlatform\Abstraction\Exceptions\ErrorException
{
    //
}
