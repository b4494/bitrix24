<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Bitrix24\ServiceProvider;
use Illuminate\Config\Repository;
use Mockery;

class CredentialsRepoTest extends TestCase
{
    private Repository|Mockery\MockInterface $config;
    private CredentialsRepo $integration_repository;

    public function setUp(): void
    {
        parent::setUp();

        $this->config = Mockery::mock(Repository::class);
        $this->config->expects('get')->with('bitrix24.applications', [])->once()->andReturn([
            [
                'id' => 'clientA',
                'client_id'     => 'local.1331221221f0e1.12111111',
                'client_secret' => 'd5168cf60409015ad9df48a25a2ffda1',
            ],

            [
                'id' => 'default',
                'client_id'     => 'app.123312343.21321321',
                'client_secret' => '8214a8af30919df2fe3c2172b59531ed',
            ]
        ]);

        $this->integration_repository = new CredentialsRepo($this->config);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->config, $this->integration_repository);
    }

    public function testGetAll(): void
    {
        $this->assertEquals([
            'clientA' => new Bitrix24AppCredentials(
                'clientA',
                clientId: 'local.1331221221f0e1.12111111',
                clientSecret: 'd5168cf60409015ad9df48a25a2ffda1'
            ),
            'default' => new Bitrix24AppCredentials(
                'default',
                clientId: 'app.123312343.21321321',
                clientSecret: '8214a8af30919df2fe3c2172b59531ed'
            ),
        ], $this->integration_repository->getAll());
    }

    public function testGetByCode(): void
    {
        $this->assertEquals(
            new Bitrix24AppCredentials(
                'clientA',
                clientId: 'local.1331221221f0e1.12111111',
                clientSecret: 'd5168cf60409015ad9df48a25a2ffda1'
            ),
            $this->integration_repository->getByCodeOrFail('clientA')
        );

        $this->assertEquals(new Bitrix24AppCredentials(
            'default',
            clientId: 'app.123312343.21321321',
            clientSecret: '8214a8af30919df2fe3c2172b59531ed'
        ), $this->integration_repository->getDefault());
    }

    public function testGetByCodeOnMissing(): void
    {
        $this->expectException(ErrorException::class);

        $this->integration_repository->getByCodeOrFail('api3');
    }
}
