<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Support\SchemaValidator;

class SchemaTest extends TestCase
{
    public function testValidator()
    {
        $schema = require __DIR__ . '/../config/bitrix24.php';

        $this->assertNull((new SchemaValidator())($schema));
    }
}
