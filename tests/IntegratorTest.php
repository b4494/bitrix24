<?php

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use BmPlatform\Bitrix24\ExtraDataProps;
use BmPlatform\Bitrix24\Integrator;
use Illuminate\Support\Facades\Config;

class IntegratorTest extends TestCase
{
    public function testIntegratesLocalApp(): void
    {
        $payload = \Mockery::mock(AppDisplayPayload::class, [
            $token = new OAuthAccessToken('access_token', 0, 'https://example.com/rest/', 'refresh_token'),
            'member_id',
            'example.com',
            'app_token',
            'L',
            'ru',
            'DEFAULT',
            [],
            false,
        ]);

        $payload->expects('getApiCommands')->andReturn($commands = \Mockery::mock(ApiCommands::class));
        $payload->makePartial();

        $commands->expects('createBot')->with('botCode', 'botName', 'email', 'url')->andReturn(123);

        Config::expects('get')->with('bitrix24.bot.email')->andReturn('email');
        Config::expects('get')->with('bitrix24.bot.url')->andReturn('url');

        $hub = \Mockery::mock(Hub::class);

        $hub->expects('integrate')->with(\Mockery::on(function ($param) use ($token) {
            $this->assertEquals(new AppIntegrationData(
                type: 'b24',
                externalId: 'member_id:botCode',
                name: 'example.com',
                locale: 'ru',
                extraData: [
                    ExtraDataProps::DOMAIN => 'example.com',
                    ExtraDataProps::BOT_ID => 123,
                    ExtraDataProps::APP_CODE => 'code',
                ],
                temporaryData: $token->toTemporaryData(),
                paymentType: PaymentType::Internal,
                status: new AppExternalStatus(AppStatus::Active),
            ), $param);

            return true;
        }))->andReturn($appInstance = \Mockery::mock(AppInstance::class));

        $integrator = new Integrator($hub,
            new Bitrix24AppCredentials('id', 'clientId', 'clientSecret', 'botCode', 'botName'),
            $payload,
            new Bitrix24AppInfo('id', 'code', '1', 'L', false, null, false, 'ru'),
        );

        $this->assertEquals($appInstance, $integrator());
    }
}