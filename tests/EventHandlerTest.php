<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\Events\CustomEventOccurred;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\EventHandler;
use BmPlatform\Bitrix24\EventHandlers\BotJoinChatHandler;
use BmPlatform\Bitrix24\EventHandlers\BotMessageAddHandler;
use BmPlatform\Bitrix24\ExtraDataProps;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event as EventFacade;
use Illuminate\Support\Facades\Log;
use Mockery;

class EventHandlerTest extends TestCase
{
    /**
     * @dataProvider eventHandlerResolveProvider
     */
    public function testEventHandlerResolve(string $event_name, string $event_handler_class, ?string $dispatch_event_class = null, $asJson = true): void
    {
        $event_handler = Mockery::mock($event_handler_class);
        $expectation = $event_handler->expects('__invoke')->once();

        if ($dispatch_event_class !== null) {
            $expectation->andReturn($eventReturn = Mockery::mock($dispatch_event_class));
        }

        $payload = new WebhookPayload($this->getWebhookPayload($event_name, asJson: $asJson));

        Log::expects('debug')->once();

        $hub = Mockery::mock(Hub::class);
        $hub->expects('findAppById')->with('b24', '9f9c63e8c5980b426e0157317330cb64:bot-marketing-bot')->zeroOrMoreTimes()->andReturn($appInstance = Mockery::mock(AppInstance::class));

        if (isset($eventReturn)) {
            $appInstance->expects('dispatchEvent')->with($eventReturn);
        }

//        $appInstance->expects('getExtraData')->andReturn([
//            ExtraDataProps::APPLICATION_TOKEN => '6a6dc89f89a00640c416639f59d6a6f2',
//        ]);

        App::expects('make')
            ->with($event_handler_class, [ 'payload' => $payload, 'appInstance' => $appInstance ])
            ->once()
            ->andReturn($event_handler);

        (new EventHandler($hub))($payload);
    }

    public function eventHandlerResolveProvider(): array
    {
        return [
//            ['ONAPPUPDATE', AppUpdateHandler::class],
            ['ONIMBOTMESSAGEADD-contact_message', BotMessageAddHandler::class, InboxReceived::class],
            ['ONIMBOTJOINCHAT', BotJoinChatHandler::class, CustomEventOccurred::class, false]
        ];
    }
}
