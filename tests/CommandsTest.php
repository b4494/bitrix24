<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\Commands;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use Mockery;

class CommandsTest extends TestCase
{
    private Commands $commands;
    private ApiCommands|Mockery\MockInterface $api_commands;

    public function setUp(): void
    {
        parent::setUp();

        $this->api_commands = Mockery::mock(ApiCommands::class);
        $this->commands     = new Commands($this->api_commands, 1, 'b24-egtff4.bitrix24.ru');
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->commands, $this->api_commands);
    }

    public function testSendTextMessage(): void
    {
        $this->api_commands->expects('sendBotMessage')->with(1, '10', 'hello')->once()->andReturn(1);

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn('10');

        $this->assertEquals(
            new MessageSendResult(externalId: '1'),
            $this->commands->sendTextMessage(new SendTextMessageRequest($chat, 'hello'))
        );
    }

    public function testSendSystemMessage(): void
    {
        $this->api_commands->expects('sendBotMessage')->with(1, '10', 'hello', null, true)->once();

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn('10');

        $this->commands->sendSystemMessage(new SendSystemMessageRequest($chat, 'hello'));
    }

    public function testSendMediaMessage(): void
    {
        $media_file = new MediaFile(MediaFileType::Image, 'https://google.com/image.jpg');

        $this->api_commands->expects('sendBotMessage')->with(1, '10', 'hello', $media_file)->once()->andReturn(1);

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn('10');

        $this->assertEquals(
            new MessageSendResult(externalId: '1'),
            $this->commands->sendMediaMessage(new SendMediaRequest($chat, $media_file, 'hello'))
        );
    }

    public function testTransferChatToOperator(): void
    {
        $result = new NewOperatorResponse(new \BmPlatform\Abstraction\DataTypes\Operator('100', ''));

        $this->api_commands->expects('transferChatToOperator')->with('10', '100')->once()->andReturn($result);
        $this->api_commands->expects('getEmployeeUserById')->with('100')->once()->andReturn(new Bitrix24User('100'));

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->once()->andReturn('10');

        $operator = Mockery::mock(Operator::class);
        $operator->expects('getExternalId')->twice()->andReturn('100');

        $this->assertEquals($result, $this->commands->transferChatToOperator(new TransferChatToOperatorRequest($chat, $operator)));
    }

    public function testTransferChatToOperatorOnOperatorNotFound(): void
    {
        $this->api_commands->expects('getEmployeeUserById')->with('100')->once()->andReturn(null);

        $chat = Mockery::mock(Chat::class);

        $operator = Mockery::mock(Operator::class);
        $operator->expects('getExternalId')->once()->andReturn('100');

        $this->expectErrorExceptionWithCode(
            ErrorCode::OperatorNotFound,
            fn () => $this->commands->transferChatToOperator(new TransferChatToOperatorRequest($chat, $operator))
        );
    }

    public function testTransferChatToOperatorOnOperatorGroup(): void
    {
        $chat = Mockery::mock(Chat::class);
        $operator_group = Mockery::mock(OperatorGroup::class);

        $this->expectErrorExceptionWithCode(
            ErrorCode::FeatureNotSupported,
            fn () => $this->commands->transferChatToOperator(new TransferChatToOperatorRequest($chat, $operator_group))
        );
    }

    public function testRestCall(): void
    {
        $context = Mockery::mock(RuntimeContext::class);
        $this->api_commands->expects('sendRestApiRequest')->with('crm.deal.get', [ 'id' => 123 ])->andReturn('result');
        $result = $this->commands->callCustomAction($context, 'restApiCall', [ 'action' => 'crm.deal.get', 'query' => [ 'id' => 123 ] ]);
        $this->assertEquals([ 'data' => 'result', 'status' => 'ok' ], $result);
    }

//    public function testUpdateContactData(): void
//    {
//        $this->api_commands->expects('updateUser')->withArgs(function (Bitrix24User $user): bool {
//            $expected_user = new Bitrix24User(
//                id: '100',
//                firstName: 'name',
//                photo: null,
//                phone: '+79991112233',
//                email: null
//            );
//
//            $this->assertEquals($expected_user, $user);
//
//            return true;
//        })->once();
//
//        $this->api_commands->expects('getContactByChatUserId')->with('100')->once()->andReturn(new Bitrix24Contact(
//            id: '6',
//            name: 'name'
//        ));
//
//        $this->api_commands->expects('updateContact')->withArgs(function (Bitrix24Contact $user): bool {
//            $expected_user = new Bitrix24Contact(
//                id: '6',
//                name: 'name',
//                emails: ['example@example.com'],
//                phones: ['+79991112233'],
//                photo_url: null,
//                comments: 'comment'
//            );
//
//            $this->assertEquals($expected_user, $user);
//
//            return true;
//        })->once();
//
//        $contact = Mockery::mock(Contact::class);
//        $contact->expects('getExternalId')->twice()->andReturn('100');
//
//        $this->commands->updateContactData(
//            new UpdateContactDataRequest(
//                contact: $contact,
//                name: 'name',
//                comment: 'comment',
//                phone: '+79991112233',
//                email: 'example@example.com'
//            )
//        );
//    }
}
