<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use Closure;
use Illuminate\Support\Facades\App;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Psr\Log\LoggerInterface;

class TestCase extends MockeryTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        App::clearResolvedInstances();
    }

    public function tearDown(): void
    {
    }

    protected function getApiResponse(string $filename): string
    {
        return $this->getPayload("ApiResponses/$filename");
    }

    protected function getWebhookPayload(string $filename, bool $asJson = false): array
    {
        $data = $this->getPayload("Webhooks/$filename");

        if ($asJson) return json_decode($data, true);

        $decoded = [];
        parse_str($data, $decoded);

        return $decoded;
    }

    private function getPayload(string $path): string
    {
        return file_get_contents(__DIR__ . "/Payloads/$path");
    }

    protected function mockLoggerIntoDI(int $times = 1): Mockery\MockInterface|LoggerInterface
    {
        return $this->mockIntoDI(LoggerInterface::class, null, null, $times);
    }

    protected function mockApiClientIntoDI(?array $args = null): Mockery\MockInterface
    {
        return $this->mockIntoDI(ApiClient::class, $args);
    }

    protected function mockIntoDI(string $class_name, ?array $args = null, ?string $alias = null, int $times = 1): Mockery\MockInterface
    {
        $object = Mockery::mock($class_name);

        $expectation = App::expects('make')->times($times)->andReturn($object);

        $alias ??= $class_name;

        if ($args === null) {
            $expectation->with($alias);
        }

        if ($args !== null) {
            $expectation->with($alias, $args);
        }

        return $object;
    }

    protected function hubExpectsFindAppById(Mockery\MockInterface $hub, ?Mockery\MockInterface $app_instance = null): void
    {
        $hub->expects('findAppById')->once()->with('b24', '0f0635897f0e6c8fb47c5d1f4f10d937')
            ->andReturn($app_instance);
    }

    protected function appInstanceExpectsGetHandler(Mockery\MockInterface $app_instance, ?int $times = 1): AppHandler|Mockery\MockInterface
    {
        $app_handler = Mockery::mock(AppHandler::class);

        $expectation = $app_instance->expects('getHandler')->times($times)->andReturn($app_handler);

        if ($times === null) {
            $expectation->zeroOrMoreTimes();
        }

        return $app_handler;
    }

    protected function appHandlerExpectsGetApplicationToken(Mockery\MockInterface $app_handler): void
    {
        $app_handler->expects('getApplicationToken')->once()->andReturn('6a6dc89f89a00640c416639f59d6a6f2');
    }

    protected function appHandlerExpectsGetDomain(Mockery\MockInterface $app_handler, ?int $times = 1): void
    {
        $expectation = $app_handler->expects('getDomain')->times($times)->andReturn('b24-egtff4.bitrix24.ru');

        if ($times === null) {
            $expectation->zeroOrMoreTimes();
        }
    }

    protected function appHandlerExpectsGetApiCommands(Mockery\MockInterface $app_handler, int $times = 1): ApiCommands|Mockery\MockInterface
    {
        $api_commands = Mockery::mock(ApiCommands::class);

        $app_handler->expects('getApiCommands')->times($times)->andReturn($api_commands);

        return $api_commands;
    }

    protected function expectErrorExceptionWithCode(ErrorCode $expected_code, Closure $closure): void
    {
        try {
            $closure();
        } catch (ErrorException $e) {
            $this->assertEquals($expected_code, $e->errorCode);

            return;
        }

        $this->fail('Missed expected Error Exception');
    }
}
