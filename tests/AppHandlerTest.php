<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\TemporaryData;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessengerType as InternalMessengerTypeEnum;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Commands;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLine;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChatPaginator;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use BmPlatform\Bitrix24\Exceptions\ErrorException;
use BmPlatform\Bitrix24\ExtraDataProps;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Illuminate\Support\Facades\Config;
use Mockery;

class AppHandlerTest extends TestCase
{
    private AppHandler $app_handler;
    private AppInstance|Mockery\MockInterface $app_instance;
    private Repository|Mockery\MockInterface $config;
    private ApiCommands|Mockery\MockInterface $api_commands;
    private ApiClient|Mockery\MockInterface $api_client;
    private CredentialsRepo|Mockery\MockInterface $credentialsRepo;

    public function setUp(): void
    {
        parent::setUp();
        $this->app_instance = Mockery::mock(AppInstance::class);
        $this->config       = Mockery::mock(Repository::class);
        Config::swap($this->config);

        $this->credentialsRepo = Mockery::mock(CredentialsRepo::class);

        $this->app_handler = new AppHandler(
            $this->app_instance,
            $this->config,
            $this->credentialsRepo,
            apiCommands: $this->api_commands = Mockery::mock(ApiCommands::class, [
                new OAuthAccessToken('access_token', 0, 'https://example.com', 'refresh_token'),
                $this->api_client = Mockery::mock(ApiClient::class),
            ]),
            apiClient: $this->api_client,
        );
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->app_handler, $this->app_instance, $this->config, $this->api_commands, $this->api_client, $this->credentialsRepo);
    }

    public function testGetMessengerInstances(): void
    {
        $this->config
            ->expects('get')
            ->with('bitrix24.messenger_types')
            ->once()
            ->andReturn($types = [
                'whatsappbytwilio' => [
                    'extends'       => 'default',
                    'name'          => 'WhatsApp by Twilio',
                    'internal_type' => 'whatsapp',
                ],
                'telegrambot' => [
                    'extends' => 'default',
                    'name' => 'Telegram',
                    'internal_type' => 'telegram',
                ],
                'livechat' => [
                    'extends' => 'default',
                    'name' => 'Live chat',
                ],
                'wz_whatsapp' => [
                    'extends' => 'default',
                    'name' => 'WZ: Whatsapp',
                    'internal_type' => 'whatsapp',
                ],
            ]);

        $this->config->expects('get')->with('bitrix24.dynamic_messenger_types')->andReturn([
            '/^(?<type>wz_(whatsapp|instagram|telegram|avito))_[a-z0-9]*$/',
        ]);

        $this->config
            ->expects('get')
            ->with('bitrix24.messenger_types.whatsappbytwilio')
            ->andReturn($types['whatsappbytwilio']);

        $this->config
            ->expects('get')
            ->with('bitrix24.messenger_types.wz_whatsapp')
            ->andReturn($types['wz_whatsapp']);

        $api_commands = $this->api_commands;

        $api_commands->expects('getActiveOpenLines')->once()->andReturn([new Bitrix24OpenLine([ 'ID' => '1', 'LINE_NAME' => 'OPEN LINE'])]);
        $api_commands->expects('checkMethodExists')->with('imconnector.list')->andReturn(true);
        $api_commands->expects('getConnectorsList')->andReturn(['whatsappbytwilio' => 'bla bla', 'telegrambot' => true, 'wz_whatsapp_cfda1d5be7e00a5d4be2447cb6fa782f3' => 'WAZZUP: WhatsApp']);
        $api_commands->expects('batchRequest')->with([
            '1_whatsappbytwilio' => [ 'method' => 'imconnector.status', 'query' => ['LINE' => '1', 'CONNECTOR' => 'whatsappbytwilio'] ],
            '1_telegrambot' => [ 'method' => 'imconnector.status', 'query' => ['LINE' => '1', 'CONNECTOR' => 'telegrambot'] ],
            '1_wz_whatsapp_cfda1d5be7e00a5d4be2447cb6fa782f3' => [ 'method' => 'imconnector.status', 'query' => ['LINE' => '1', 'CONNECTOR' => 'wz_whatsapp_cfda1d5be7e00a5d4be2447cb6fa782f3'] ],
        ])->andReturn([
            '1_whatsappbytwilio' => [ 'STATUS' => true ],
            '1_telegram' => [ 'STATUS' => false ],
            '1_wz_whatsapp_cfda1d5be7e00a5d4be2447cb6fa782f3' => [ 'STATUS' => true ],
        ]);

        $this->assertEquals(new IterableData([
            new MessengerInstance(
                externalType: 'whatsappbytwilio',
                externalId: '1|whatsappbytwilio',
                name: 'OPEN LINE',
                internalType: InternalMessengerTypeEnum::Whatsapp,
            ),

            new MessengerInstance(
                externalType: 'wz_whatsapp',
                externalId: '1|wz_whatsapp_cfda1d5be7e00a5d4be2447cb6fa782f3',
                name: 'OPEN LINE',
                internalType: InternalMessengerTypeEnum::Whatsapp,
            )
        ], null), $this->app_handler->getMessengerInstances());
    }

//    public function testGetChats(): void
//    {
//        $this->appInstanceExpectsGetExtraData($this->app_instance, 2);
//
//        $this->config->expects('get')->with('bitrix24.messenger_types')->once()->andReturn([
//            ExternalMessengerTypeEnum::TELEGRAM_BOT->value => [
//                'extends'       => 'default',
//                'name'          => 'Telegram',
//                'internal_type' => InternalMessengerTypeEnum::Telegram->value
//            ],
//            ExternalMessengerTypeEnum::LIVE_CHAT->value => [
//                'extends' => 'default',
//                'name'    => 'Онлайн-чат',
//            ]
//        ]);
//
//        $api_commands = $this->api_commands;
//
//        $api_commands->expects('getRecentOpenLineChats')
//            ->once()
//            ->andReturn(
//                new Bitrix24OpenLineChatPaginator(
//                    items: [
//                        new Bitrix24OpenLineChat(
//                            id: 22,
//                            openLineId: '2',
//                            messengerType: 'telegrambot',
//                            startUserId: '74', userCounter: Carbon::parse('2022-10-17 13:08:57', '+3')
//                        ),
//                        new Bitrix24OpenLineChat(
//                            id: 22,
//                            openLineId: '2',
//                            messengerType: 'telegrambot',
//                            startUserId: '74',
//                            userCounter: 3
//                        ),
//                    ],
//                    has_more: true
//                )
//            );
//
//        $api_commands->expects('getContactByChatUserId')
//            ->with('74')
//            ->twice()
//            ->andReturns(
//                new Bitrix24Contact(
//                    id: '74'
//                )
//            );
//
//        $api_commands->expects('getChatOperatorById')
//            ->with(22, 1, '74')
//            ->once()
//            ->andReturns(
//                new Bitrix24User(
//                    id: '10'
//                )
//            );
//
//        $this->assertEquals(new IterableData([
//            new Chat(
//                externalId: '22',
//                messengerInstance: new MessengerInstance(
//                    externalType: ExternalMessengerTypeEnum::TELEGRAM_BOT->value,
//                    externalId: '2|telegrambot',
//                    name: 'Telegram',
//                    description: 'Telegram',
//                    messengerId: null,
//                    internalType: InternalMessengerTypeEnum::Telegram
//                ),
//                contact: '74',
//                operator: null,
//                messengerId: null
//            ),
//            new Chat(
//                externalId: '22',
//                messengerInstance: new MessengerInstance(
//                    externalType: ExternalMessengerTypeEnum::TELEGRAM_BOT->value,
//                    externalId: '2|telegrambot',
//                    name: 'Telegram',
//                    description: 'Telegram',
//                    messengerId: null,
//                    internalType: InternalMessengerTypeEnum::Telegram
//                ),
//                contact: '74',
//                operator: new Operator(
//                    externalId: '10',
//                    name: ''
//                ),
//                messengerId: null
//            ),
//        ], '2022-10-17'), $this->app_handler->getChats());
//    }

    public function testGetOperators(): void
    {
        $api_commands = $this->api_commands;

        $api_commands->expects('getEmployeeUsers')->andReturn([
            new Bitrix24User(
                id: '1',
                firstName: 'name',
                lastName: 'lname',
                photo: 'photo',
                phone: '99999',
                email: 'example@example.com'
            )
        ]);

        $api_commands->expects('getCursor')->andReturn(50);

        $this->assertEquals(new IterableData([
            new Operator(
                externalId: '1',
                name: 'name lname',
                phone: '99999',
                email: 'example@example.com',
                avatarUrl: 'photo',
                extraData: [
                    ExtraDataProps::FIRST_NAME => 'name',
                    ExtraDataProps::LAST_NAME => 'lname',
                ],
            )
        ], 50), $this->app_handler->getOperators());
    }

//    public function testGetCommands(): void
//    {
//        $this->appInstanceExpectsGetTemporaryData($this->app_instance);
//        $this->appInstanceExpectsGetExtraData($this->app_instance, 2);
//        $api_client = $this->mockApiClientIntoDI();
//
//        $this->mockAppCommandsWithData($api_client, $this->app_handler);
//
//        $this->assertInstanceOf(Commands::class, $this->app_handler->getCommands());
//    }
//
//    public function testGetApiCommands(): void
//    {
//        $this->appInstanceExpectsGetTemporaryData($this->app_instance);
//
//        $api_client = $this->mockApiClientIntoDI();
//
//        $this->mockAppCommandsWithData($api_client, $this->app_handler);
//
//        $this->assertInstanceOf(ApiCommands::class, $this->app_handler->getApiCommands());
//    }

    public function testGetApiCommandsOnNullTemporaryData(): void
    {
        $this->appInstanceExpectsGetTemporaryData($this->app_instance, false);
        $this->expectErrorExceptionWithCode(ErrorCode::AuthenticationFailed, fn () => $this->app_handler->getOAuthAccessToken());
    }

    public function testFreshTemporaryData(): void
    {
        $data = $this->getTestTemporaryData();

        $handler = Mockery::mock(AppHandler::class, [
            $this->app_instance,
            $this->config,
            $this->credentialsRepo,
            $this->api_commands,
            Mockery::mock(Commands::class),
            $this->api_client,
        ]);
        $handler->makePartial();
        $handler->expects('getOAuthCredentials')->andReturn(new Bitrix24AppCredentials('default', 'local.123.123', 'abc'));

        $this->api_client->expects('sendOAuthRequest')->with([
            'grant_type' => 'refresh_token',
            'client_id' => 'local.123.123',
            'client_secret' => 'abc',
            'refresh_token' => 'refresh_token',
        ])->andReturn([
            'access_token' => 'access_token',
            'refresh_token' => 'refresh_token',
            'client_endpoint' => 'https://example.com',
            'expires' => $data->expiresAt->timestamp,
        ]);

        $this->assertEquals($data, $handler->freshTemporaryData($data));
    }

    public function testGetAppInstance(): void
    {
        $this->assertEquals($this->app_instance, $this->app_handler->getAppInstance());
    }

    public function testGetOAuthClient(): void
    {
        $this->credentialsRepo->expects('getByCodeOrFail')->with('local')->andReturn(new Bitrix24AppCredentials(
            'default',
            clientId: 'local.123.123',
            clientSecret: 'abc'
        ));

        $this->app_instance->expects('getExtraData')->andReturn([ ExtraDataProps::APP_CODE => 'local' ]);

        $this->assertEquals(new Bitrix24AppCredentials('default', 'local.123.123', 'abc'), $this->app_handler->getOAuthCredentials());
    }

    public function testGetOAuthAccessToken(): void
    {
        $this->appInstanceExpectsGetTemporaryData($this->app_instance);

        $this->assertEquals(new OAuthAccessToken(
            accessToken: 'access_token',
            expires: 1577836800,
            clientEndpoint: 'https://example.com',
            refreshToken: 'refresh_token'
        ), $this->app_handler->getOAuthAccessToken());
    }

    /** @dataProvider statuses */
    public function testGetStatus($status, $response): void
    {
        $this->api_commands->expects('appInfo')->andReturn($response);
        $this->assertEquals($status, $this->app_handler->status());
    }

    public function statuses(): array
    {
        return [
            [ new AppExternalStatus(AppStatus::Active), new Bitrix24AppInfo('id', 'code', '1', 'F', false, 0, true, 'ru') ],
            [ new AppExternalStatus(AppStatus::Active), new Bitrix24AppInfo('id', 'code', '1', 'L', false, 0, true, 'ru') ],
            [
                new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid, now()->endOfDay()->addDays(30)),
                new Bitrix24AppInfo('id', 'code', '1', 'P', false, 30, true, 'ru'),
            ],
            [
                new AppExternalStatus(AppStatus::Active, PaymentStatus::Paid, now()->endOfDay()->addDays(30)),
                new Bitrix24AppInfo('id', 'code', '1', 'S', false, 30, true, 'ru'),
            ],
            [
                new AppExternalStatus(AppStatus::Active, PaymentStatus::Trial, now()->endOfDay()->addDays(7)),
                new Bitrix24AppInfo('id', 'code', '1', 'T', false, 7, true, 'ru'),
            ],
            [
                new AppExternalStatus(AppStatus::Frozen),
                new Bitrix24AppInfo('id', 'code', '1', 'P', true, -3, true, 'ru'),
            ],
        ];
    }

    public function testStatusOnPaymentExpired(): void
    {
        $this->api_commands->expects('appInfo')->andThrow(new ErrorException(ErrorCode::PaymentExpired));

        $this->assertEquals(new AppExternalStatus(AppStatus::Frozen), $this->app_handler->status());
    }

    public function testGetBotId(): void
    {
        $this->appInstanceExpectsGetExtraData($this->app_instance);

        $this->assertEquals(1, $this->app_handler->getBotId());
    }

    public function testGetDomain(): void
    {
        $this->appInstanceExpectsGetExtraData($this->app_instance);

        $this->assertEquals('b24-egtff4.bitrix24.ru', $this->app_handler->getDomain());
    }

    protected function mockAppCommandsWithData(Mockery\MockInterface $api_client, AppHandler $app_handler): Mockery\MockInterface
    {
        return parent::mockIntoDI(ApiCommands::class, [
            'token' => new OAuthAccessToken('access_token', 0, 'https://example.com', 'refresh_token'),
            'client'          => $api_client,
            'app_handler'     => $app_handler
        ]);
    }

    private function appInstanceExpectsGetTemporaryData(Mockery\MockInterface $app_instance, bool $return_data = true)
    {
        $app_instance->expects('getTemporaryData')
            ->once()
            ->andReturn($return_data ? $this->getTestTemporaryData() : null);
    }

    private function appInstanceExpectsGetExtraData(Mockery\MockInterface $app_instance, int $times = 1)
    {
        $app_instance->expects('getExtraData')->times($times)->andReturn([
            ExtraDataProps::BOT_ID            => 1,
            ExtraDataProps::DOMAIN            => 'b24-egtff4.bitrix24.ru'
        ]);
    }

    private function getTestTemporaryData(): TemporaryData
    {
        $date = Carbon::parse('2020-01-01 00:00:00');

        return new TemporaryData('access_token;https://example.com;refresh_token', $date);
    }

    public function testGetsMemberId(): void
    {
        $handler = new AppHandler($this->app_instance, $this->config, $this->credentialsRepo);
        $this->app_instance->expects('getExternalId')->andReturn('member_id:123');
        $this->assertEquals('member_id', $handler->getMemberId());
    }

    public function testGetsCorrectAppCredentials(): void
    {
        $handler = new AppHandler($this->app_instance, $this->config, $this->credentialsRepo);

        $this->app_instance->expects('getExtraData')->andReturn([ ExtraDataProps::APP_CODE => 'local' ]);
        $this->credentialsRepo->expects('getByCodeOrFail')->with('local')->andReturn($localCreds = new Bitrix24AppCredentials('id', 'localId', 'localSecret'));
        $this->assertEquals($localCreds, $handler->getOAuthCredentials());

        $this->app_instance->expects('getExtraData')->andReturn([ ExtraDataProps::APP_CODE => 'defaultApp' ]);
        $this->credentialsRepo->expects('getByCodeOrFail')->with('defaultApp')->andReturn($defaultApp = new Bitrix24AppCredentials('id', 'defaultId', 'localId'));
        $this->assertEquals($defaultApp, $handler->getOAuthCredentials());
    }
}
