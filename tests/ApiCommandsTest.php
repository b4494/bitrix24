<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Bot;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLine;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChat;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24OpenLineChatPaginator;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24ChatUser;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Contact;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24ContactMessenger;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use Carbon\Carbon;
use Closure;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Support\Facades\App;
use Mockery;
use Psr\Log\LoggerInterface;

class ApiCommandsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    public function testCreateBot(): void
    {
        $url = $this->mockIntoDI(UrlGenerator::class, null, 'url');
        $url->expects('route')->andReturn('/abc');

        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imbot.register?auth=123', [
                'CODE' => 'code',
                'TYPE' => 'O',
                'EVENT_HANDLER' => '/abc',
                'PROPERTIES' => [
                    'NAME' => 'name',
                    'COLOR' => 'LIGHT_BLUE',
                    'EMAIL' => 'email',
                    'PERSONAL_WWW' => 'url',
                ],
            ]);

            return new Response(200, [], $this->getApiResponse('imbot.register'));
        });

        $api_commands->createBot('code', 'name', 'email', 'url');
    }

    public function testGetBots(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imbot.bot.list?auth=123');

            return new Response(200, [], $this->getApiResponse('imbot.bot.list'));
        });

        $bots = $api_commands->getBots();

        $this->assertEquals([
            new Bitrix24Bot(2, 'marta', 'Марта'),
            new Bitrix24Bot(44, '58611331-d65e-4881-bef6-9bf1c053548e', 'BotMarketing')
        ], $bots);
    }

    public function testSendBotMessage(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imbot.message.add?auth=123', [
                'BOT_ID' => '1',
                'DIALOG_ID' => 'chat2',
                'MESSAGE' => 'Hola!',
                'SYSTEM' => 'N'
            ]);

            return new Response(200, [], $this->getApiResponse('imbot.message.add'));
        });

        $result = $api_commands->sendBotMessage(1, '2', 'Hola!');

        $this->assertEquals(418, $result);
    }

    public function testSendBotMessageOnSystem(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imbot.message.add?auth=123', [
                'BOT_ID' => '1',
                'DIALOG_ID' => 'chat2',
                'MESSAGE' => 'Hola!',
                'SYSTEM' => 'Y'
            ]);

            return new Response(200, [], $this->getApiResponse('imbot.message.add'));
        });

        $result = $api_commands->sendBotMessage(1, '2', 'Hola!', null, true);

        $this->assertEquals(418, $result);
    }

    public function testSendBotMessageOnMedia(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imbot.message.add?auth=123', [
                'BOT_ID' => '1',
                'DIALOG_ID' => 'chat2',
                'MESSAGE' => 'Hola!',
                'ATTACH' => [
                    [
                        'IMAGE' => [
                            'LINK' => 'https://example.com/image.jpg'
                        ]
                    ]
                ],
                'SYSTEM' => 'N'
            ]);

            return new Response(200, [], $this->getApiResponse('imbot.message.add'));
        });

        $result = $api_commands->sendBotMessage(1, '2', 'Hola!', new MediaFile(MediaFileType::Image, 'https://example.com/image.jpg'));

        $this->assertEquals(418, $result);
    }

    public function testTransferChatToOperator(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imopenlines.bot.session.transfer?auth=123', [
                'CHAT_ID' => '1',
                'USER_ID' => '2',
                'LEAVE' => 'N',
            ]);

            return new Response(200, [], $this->getApiResponse('imopenlines.bot.session.transfer'));
        });

        $api_commands->transferChatToOperator('1', '2');
    }

    public function testGetEmployeeUsers(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/user.get?auth=123', [
                'USER_TYPE' => 'employee',
                'start' => 0,
            ]);

            return new Response(200, [], $this->getApiResponse('user.get'));
        });

        $employees = $api_commands->getEmployeeUsers();

        $this->assertEquals([
            new Bitrix24User('1', '', '', null, null, 'botmarketingdev@gmail.com'),
        ], $employees);
    }

    public function testGetEmployeeUserById(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/user.get?auth=123', [
                'USER_TYPE' => 'employee',
                'ID' => 1
            ]);

            return new Response(200, [], $this->getApiResponse('user.get-id_filter'));
        });

        $this->assertEquals(
            new Bitrix24User('1', '', '', null, null, 'botmarketingdev@gmail.com'),
            $api_commands->getEmployeeUserById('1')
        );
    }

    public function testGetEmployeeUserByIdOnNotFound(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/user.get?auth=123', [
                'USER_TYPE' => 'employee',
                'ID' => 1
            ]);

            return new Response(200, [], $this->getApiResponse('user.get-id_filter_empty'));
        });

        $this->assertNull($api_commands->getEmployeeUserById('1'));
    }

    public function testGetChatUserById(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/im.user.get?auth=123', [
                'ID' => '40',
            ]);

            return new Response(200, [], $this->getApiResponse('im.user.get'));
        });

        $this->assertEquals(
            new Bitrix24ChatUser(
                id: 40,
                name: 'V',
                avatar: 'https://b24-egtff4.bitrix24.ru/b22778150\/resize_cache\/144\/ff58db95aecdfa09ae61b51b5fd8f63f\/main\/2ed\/2ed5c1d4792305a94308a60cd5be0fac\/2e6f18d186f1e2cb7dbaef44ffe6d100.jpg'
            ),
            $api_commands->getChatUserById(40)
        );
    }

    public function testGetChatUserByIdOnNotFound(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/im.user.get?auth=123', [
                'ID' => '40',
            ]);

            return new Response(200, [], $this->getApiResponse('im.user.get-empty'));
        });

        $this->assertEquals(null, $api_commands->getChatUserById(40));
    }

    public function testGetChatUserIds(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/im.chat.user.list?auth=123', [
                'CHAT_ID' => '20',
            ]);

            return new Response(200, [], $this->getApiResponse('im.chat.user.list'));
        });

        $this->assertEquals(
            [1, 40, 68],
            $api_commands->getChatUserIds(20)
        );
    }

    public function testGetChatOperatorById(): void
    {
        $api_commands = $this->makeApiCommands(
            null, fn() => new Response(200, [], $this->getApiResponse('im.chat.user.list')),
            function (Request $request) {
                $this->assertRequestContains($request, ':b24endpoint/user.get?auth=123', [
                    'USER_TYPE' => 'employee',
                    'ID'        => 1
                ]);

                return new Response(200, [], $this->getApiResponse('user.get'));
            }
        );

        $this->assertEquals(
            new Bitrix24User('1', '', '', null, null, 'botmarketingdev@gmail.com'),
            $api_commands->getChatOperatorById(20, 68, '40')
        );
    }

    public function testGetChatOperatorByIdOnNoOperator(): void
    {
        $api_commands = $this->makeApiCommands(null,
            fn() => new Response(200, [], $this->getApiResponse('im.chat.user.list-two_persons'))
        );

        $this->assertNull($api_commands->getChatOperatorById(20, 68, '40'));
    }

    public function testUpdateUser(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/user.update?auth=123', [
                'ID' => '40',
                'NAME' => '123'
            ]);

            return new Response(200, [], $this->getApiResponse('user.update'));
        });

        $api_commands->updateUser(new Bitrix24User(
            id: '40',
            firstName: '123',
            photo: '123',
            email: 'test@test.com'
        ));
    }

    public function testGetOpenLineIds(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imopenlines.config.list.get?auth=123');

            return new Response(200, [], $this->getApiResponse('imopenlines.config.list.get'));
        });

        $this->assertEquals(['2', '4'], $api_commands->getOpenLineIds());
    }

    public function testGetActiveOpenLines(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imopenlines.config.list.get?auth=123', [
                'PARAMS' => [
                    'select' => [ 'ID', 'LINE_NAME' ],
                    'filter' => [ 'ACTIVE' => 'Y' ],
                ],
            ]);

            return new Response(200, [], $this->getApiResponse('imopenlines.config.list.get-select'));
        });

        $this->assertEquals([
            new Bitrix24OpenLine([
                'ID' => '1',
                'LINE_NAME' => 'Открытая линия Bot-Marketing',
            ]),
            new Bitrix24OpenLine([
                'ID' => '3',
                'LINE_NAME' => 'Открытая линия 2',
            ]),
        ], $api_commands->getActiveOpenLines([ 'ID', 'LINE_NAME' ]));
    }

    public function testIsOpenLineConnectorConnected(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imconnector.status?auth=123', [
                'LINE' => '2',
                'CONNECTOR' => 'telegrambot'
            ]);

            return new Response(200, [], $this->getApiResponse('imconnector.status'));
        });

        $this->assertTrue($api_commands->isOpenLineConnectorConnected('2', 'telegrambot'));
    }

    public function testSetOpenLineBot(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/imopenlines.config.update?auth=123', [
                'CONFIG_ID' => '2',
                'PARAMS' => [
                    'WELCOME_BOT_ENABLE' => 'Y',
                    'WELCOME_BOT_JOIN' => 'always',
                    'WELCOME_BOT_ID' => 1
                ]
            ]);

            return new Response(200, [], $this->getApiResponse('imconnector.status'));
        });

        $api_commands->setOpenLineBot('2', 1);
    }

    public function testGetContactByChatUserId(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/crm.contact.list?auth=123', [
                'filter' => [ 'CREATED_BY_ID' => 40 ],
                'select' => [ 'ID', 'NAME', 'EMAIL', 'WEB', 'PHONE', 'PHOTO', 'COMMENTS' ]
            ]);

            return new Response(200, [], $this->getApiResponse('crm.contact.list'));
        });

        $this->assertEquals(new Bitrix24Contact(
            id: '6',
            name: 'V',
            emails: ['example@example.com'],
            phones: ['9991111111'],
            photo_url: '/bitrix/components/bitrix/crm.contact.show/show_file.php?ownerId=6&fieldName=PHOTO&dynamic=N&fileId=240',
            messengers: [
                new Bitrix24ContactMessenger(ExternalMessengerTypeEnum::TELEGRAM_BOT, 'hello')
            ]
        ), $api_commands->getContactByChatUserId('40'));
    }

    public function testGetContactByChatUserIdOnNotFound(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/crm.contact.list?auth=123', [
                'filter' => [ 'CREATED_BY_ID' => 40 ],
                'select' => [ 'ID', 'NAME', 'EMAIL', 'WEB', 'PHONE', 'PHOTO', 'COMMENTS' ]
            ]);

            return new Response(200, [], $this->getApiResponse('crm.contact.list-empty'));
        });

        $this->assertNull($api_commands->getContactByChatUserId('40'));
    }

    public function testUpdateContact(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/crm.contact.update?auth=123', [
                'ID' => 6,
                'FIELDS' => [
                    'NAME' => 'V',
                    'COMMENTS' => 'comment',
                    'PHONE' => [ [ 'VALUE' => '9991111111' ] ],
                    'EMAIL' => [ [ 'VALUE' => 'example@example.com' ] ],
                ]
            ]);

            return new Response(200, [], $this->getApiResponse('crm.contact.update'));
        });

        $api_commands->updateContact(new Bitrix24Contact(
            id: '6',
            name: 'V',
            emails: ['example@example.com'],
            phones: ['9991111111'],
            comments: 'comment',
        ));
    }

    public function testGetRecentChats(): void
    {
        $api_commands = $this->makeApiCommands(null, function (Request $request) {
            $this->assertRequestContains($request, ':b24endpoint/im.recent.list?auth=123', [
                'SKIP_DIALOG' => 'Y',
                'SKIP_CHAT' => 'Y',
                'SKIP_OPENLINES' => 'N'
            ]);

            return new Response(200, [], $this->getApiResponse('im.recent.list'));
        });

        $response = $api_commands->getRecentOpenLineChats();

        $this->assertEquals(new Bitrix24OpenLineChatPaginator(
            items: [
                new Bitrix24OpenLineChat(
                    id: 22,
                    openLineId: 2,
                    messengerType: 'fbinstagramdirect',
                    externalUserId: '5566369343430302',
                    startUserId: 74,
                    authorId: 0,
                    entityRefs: [
                      'LEAD' => 0,
                      'COMPANY' => 0,
                      'CONTACT' => 12,
                      'DEAL' => 34,
                    ],
                    userCounter: 3
                ),
                new Bitrix24OpenLineChat(
                    id: 20,
                    openLineId: 4,
                    messengerType: 'telegrambot',
                    externalUserId: '304322035',
                    startUserId: 40,
                    authorId: 0,
                    entityRefs: [
                        'LEAD' => 0,
                        'COMPANY' => 0,
                        'CONTACT' => 6,
                        'DEAL' => 28,
                    ],
                    userCounter: 3
                ),
            ],
            has_more: false
        ), $response);
    }

    public function testGetBotsOnTokenExpire(): void
    {
        $api_commands = $this->makeApiCommands(
            $app = Mockery::mock(AppHandler::class),
            fn () => new Response(401, [], '{"error":"expired_token","error_description":"The access token provided has expired."}'),
//            fn () => new Response(200, [], $this->getApiResponse('oauth_token_refresh_token')),
            fn () => new Response(200, [], $this->getApiResponse('imbot.bot.list'))
        );

        $app->expects('getOAuthAccessToken')->with(true)->andReturn(
            new OAuthAccessToken('123', 0, 'https://example.com', '123'),
        );

        $bots = $api_commands->getBots();

        $this->assertEquals([
            new Bitrix24Bot(2, 'marta', 'Марта'),
            new Bitrix24Bot(44, '58611331-d65e-4881-bef6-9bf1c053548e', 'BotMarketing')
        ], $bots);
    }

    private function makeApiCommands($app, Closure ...$response_closure): ApiCommands
    {
        $handler = new MockHandler($response_closure);

        return new ApiCommands(
            token: new OAuthAccessToken('123', 0, 'https://example.com', '123'),
            client: new ApiClient(
                options: [ 'handler' => $handler ]
            ),
            app: $app,
        );
    }

    private function assertRequestContains(Request $request, string $url, array $body = [], string $method = 'POST')
    {
        $this->assertEquals($method, $request->getMethod());
        $this->assertEquals($url, (string) $request->getUri());
        $this->assertEquals(http_build_query($body), $request->getBody()->getContents());
    }

    private function mockAppHandlerExpectsRefreshToken(): Mockery\MockInterface|AppHandler
    {
        $mock = Mockery::mock(AppHandler::class);
        $mock->expects('getOAuthClient')->twice()->andReturn(
            new OAuthClient('10', '20')
        );

        $mock->expects('getRefreshToken')->once()->andReturn('30');
        $mock->expects('refreshTemporaryData')->once();

        return $mock;
    }

    public function testMethodExistence(): void
    {
        $token = Mockery::mock(OAuthAccessToken::class);
        $client = Mockery::mock(ApiClient::class);

        $commands = new ApiCommands($token, $client);
        $client->expects('sendRestApiRequest')->with($token, 'method.get', [ 'name' => 'test' ])
            ->andReturn([ 'result' => [ 'isExisting' => true, 'isAvailable' => true ]]);
        $this->assertTrue($commands->checkMethodExists('test'));

        $client->expects('sendRestApiRequest')->with($token, 'method.get', [ 'name' => 'otherTest' ])
            ->andReturn([ 'result' => [ 'isExisting' => true, 'isAvailable' => false ]]);
        $this->assertFalse($commands->checkMethodExists('otherTest'));
    }
}
