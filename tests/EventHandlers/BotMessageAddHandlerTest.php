<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\Entities\Webhook\AuthPayload;
use BmPlatform\Bitrix24\Entities\Webhook\BotPayload;
use BmPlatform\Bitrix24\Entities\Webhook\WebhookPayload;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;
use BmPlatform\Bitrix24\EventHandlers\BotMessageAddHandler;
use BmPlatform\Bitrix24\ExtraDataProps;
use BmPlatform\Bitrix24\Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Config\Repository;
use Mockery;

class BotMessageAddHandlerTest extends TestCase
{
    private AppInstance|Mockery\MockInterface $app_instance;
    private AppHandler|Mockery\MockInterface $app_handler;
    private Hub|Mockery\MockInterface $hub;
    private Repository|Mockery\MockInterface $config;

    public function setUp(): void
    {
        parent::setUp();

        \Illuminate\Support\Facades\Config::swap($this->config = Mockery::mock(Repository::class));

        $this->config->expects('get')->with('bitrix24.messenger_types.telegrambot')->andReturn([
            'extends'       => 'default',
            'name'          => 'Telegram',
            'internal_type' => MessengerType::Telegram->value
        ])->zeroOrMoreTimes();

        $this->app_instance = Mockery::mock(AppInstance::class);
        $this->hub = Mockery::mock(Hub::class);

        $this->app_handler = $this->appInstanceExpectsGetHandler($this->app_instance, null);

        $this->app_handler->expects('getApplicationToken')->andReturn('6a6dc89f89a00640c416639f59d6a6f2')->zeroOrMoreTimes();

        $this->appHandlerExpectsGetDomain($this->app_handler, null);

        $this->hub->expects('findAppById')->zeroOrMoreTimes()->with('b24', '0f0635897f0e6c8fb47c5d1f4f10d937')
            ->andReturn($this->app_instance);
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unset($this->app_instance, $this->app_handler, $this->hub, $this->config);
    }

    public function testInvokeOnContactMessage(): void
    {
        $expected_event = new InboxReceived(
            chat: new Chat(
                externalId: '4119',
                messengerInstance: '3|telegrambot',
                contact: '975',
                messengerId: '192107759',
                extraData: [
                    ExtraDataProps::CONTACT_ID => 0,
                    ExtraDataProps::DEAL_ID => 0,
                    ExtraDataProps::COMPANY_ID => 0,
                    ExtraDataProps::LEAD_ID => 5267,
                ],
            ),
            participant: new Contact(
                externalId: '975',
                name: 'Alexander Kalnoy',
                extraData: [
                    'fn' => 'Alexander',
                    'ln' => 'Kalnoy',
                ],
            ),
            message: new MessageData('281497', 'привет', []),
            timestamp: Carbon::createFromTimestamp(1682322301),
        );

        $this->invokeOnContactMessage('ONIMBOTMESSAGEADD-contact_message', $expected_event);
    }

    public function testInvokeOnContactMessageWithImage(): void
    {
        $expected_event = new InboxReceived(
            chat: new Chat(
                externalId: '4119',
                messengerInstance: '3|telegrambot',
                contact: '975',
                messengerId: '192107759',
                extraData: [
                    ExtraDataProps::CONTACT_ID => 0,
                    ExtraDataProps::DEAL_ID => 0,
                    ExtraDataProps::COMPANY_ID => 0,
                    ExtraDataProps::LEAD_ID => 5267,
                ],
            ),
            participant: new Contact(
                externalId: '975',
                name: 'Alexander Kalnoy',
                extraData: [
                    'fn' => 'Alexander',
                    'ln' => 'Kalnoy',
                ],
            ),
            message: new MessageData('281477', null, [
                new MediaFile(
                    type: MediaFileType::Image,
                    url: 'url',
                    mime: 'image/png',
                    name: 'image_2023-04-24_09-59-41 (1).png',
                )
            ]),
            timestamp: Carbon::createFromTimestamp(1682321092),
        );

        $payload = Mockery::mock(WebhookPayload::class, [ $this->getWebhookPayload('ONIMBOTMESSAGEADD-contact_message_image', asJson: true) ]);
        $payload->makePartial();

        $payload->expects('getBotPayload')->andReturn(new BotPayload(
            123,
            'bot',
            $authPayload = Mockery::mock(AuthPayload::class),
        ));

        $authPayload->expects('getApiCommands')->andReturn($apiCommands = Mockery::mock(ApiCommands::class));

        $apiCommands->expects('batchRequest')->with([ '69803' => [ 'method' => 'disk.file.get', 'query' => [ 'id' => 69803 ]] ])->andReturn([
            '69803' => [ 'DOWNLOAD_URL' => 'url' ],
        ]);

        $event_handler = new BotMessageAddHandler(
            $this->app_instance,
            $payload,
        );
        $event = $event_handler();

        $this->assertEquals($expected_event, $event);
    }

    private function invokeOnContactMessage(string $webhook_filename, InboxReceived $expected_event): void
    {
        $event_handler = new BotMessageAddHandler(
            $this->app_instance,
            new WebhookPayload($this->getWebhookPayload($webhook_filename, asJson: true)),
        );
        $event = $event_handler();

        $this->assertEquals($expected_event, $event);
    }

    public function testInvokeOnOperatorMessage(): void
    {
        $event_handler = new BotMessageAddHandler(
            $this->app_instance,
            new WebhookPayload($this->getWebhookPayload('ONIMBOTMESSAGEADD-operator_message', asJson: true)),
        );
        $event = $event_handler();

        $expected_event = new OutboxSent(
            chat: new Chat(
                externalId: '4119',
                messengerInstance: '3|telegrambot',
                contact: '975',
                operator: '19',
                messengerId: '192107759',
                extraData: [
                    ExtraDataProps::CONTACT_ID => 0,
                    ExtraDataProps::DEAL_ID => 0,
                    ExtraDataProps::COMPANY_ID => 0,
                    ExtraDataProps::LEAD_ID => 5267,
                ],
            ),
            message: new MessageData('281501', 'йоу', []),
            operator: new Operator('19', 'Александр Кальной', extraData: [ ExtraDataProps::FIRST_NAME => 'Александр', ExtraDataProps::LAST_NAME => 'Кальной']),
            timestamp: Carbon::createFromTimestamp(1682323030),
        );

        $this->assertEquals($expected_event, $event);
    }

    public function testInvokeOnMessageToInternalBitrixChatFromOperator(): void
    {
        $event_handler = new BotMessageAddHandler(
            $this->app_instance,
            new WebhookPayload($this->getWebhookPayload('ONIMBOTMESSAGEADD-non_openlines')),
        );

        $event = $event_handler();

        $this->assertNull($event);
    }
}
