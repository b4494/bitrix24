<?php

include __DIR__.'/../../../../vendor/autoload.php';

foreach ((new Illuminate\Filesystem\Filesystem())->files(__DIR__.'/../') as $file) {
    parse_str(file_get_contents($file->getRealPath()), $data);
    file_put_contents(__DIR__.'/'.$file->getFilename().'.json', json_encode($data, JSON_UNESCAPED_UNICODE));
}