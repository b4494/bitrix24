<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Bitrix24\ApiClient;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use Closure;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\App;
use Psr\Log\LoggerInterface;

class ApiClientTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testSendOAuthRequestOnSuccess(): void
    {
        $response = $this->makeApiClient(function (Request $request) {
            $this->assertEquals('GET', $request->getMethod());
            $this->assertEquals('https://oauth.bitrix.info/oauth/token/?grant_type=refresh_token&client_id=1&client_secret=secret&refresh_token=refresh', (string) $request->getUri());

            return new Response(200, [], '{"never_gonna_say":"good_bye"}');
        })->sendOAuthRequest([
            'grant_type'    => 'refresh_token',
            'client_id'     => '1',
            'client_secret' => 'secret',
            'refresh_token' => 'refresh',
        ]);

        $this->assertEquals([
            'never_gonna_say' => 'good_bye'
        ], $response);
    }

    public function testSendRestApiRequestOnSuccess(): void
    {
        $response = $this->makeApiClient(function (Request $request) {
            $this->assertEquals('POST', $request->getMethod());
            $this->assertEquals(':b24endpoint/never.gonna.give.you.up?auth=123', (string) $request->getUri());
            $this->assertEquals(http_build_query([
                'never_gonna' => ' let_you_down'
            ]), $request->getBody()->getContents());

            return new Response(200, [], '{"result":{"never_gonna_say":"good_bye"}}');
        })->sendRestApiRequest(new OAuthAccessToken('123', 0, 'https://example.com', '123'), 'never.gonna.give.you.up', [
            'never_gonna' => ' let_you_down'
        ]);

        $this->assertEquals([
            'result' => [
                'never_gonna_say' => 'good_bye'
            ]
        ], $response);
    }

    /**
     * @dataProvider sendRestApiRequestOnErrorProvider
     */
    public function testSendRestApiRequestOnError(ErrorCode $expected_code, int $response_code, string $body): void
    {
        $this->expectErrorExceptionWithCode($expected_code, function () use ($response_code, $body) {
            $this->makeApiClient(fn () => new Response($response_code, [], $body))
                ->sendRestApiRequest(new OAuthAccessToken('123', 0, 'https://example.com', '123'), 'never.gonna.give.you.up', [
                    'never_gonna' => ' let_you_down'
                ]);
        });
    }

    public function sendRestApiRequestOnErrorProvider(): array
    {
        return [
            [ErrorCode::IntegrationNotPossible, 400, '{"error":"MAX_COUNT_ERROR","error_description":"Has reached the maximum number of bots for application (max: 5)"}'],
            [ErrorCode::InternalServerError, 500, '{"error":"INTERNAL_SERVER_ERROR","error_description":"Internal server error"}'],
            [ErrorCode::ServiceUnavailable, 502, ''],
            [ErrorCode::NotFound, 400, '{"error":"BOT_ID_ERROR"}'],
            [ErrorCode::DataMissing, 400, '{"error":"MESSAGE_EMPTY"}'],
            [ErrorCode::UnexpectedServerError, 400, '{"error":"UNEXPECTED_ERROR"}']
        ];
    }

    private function makeApiClient(Closure ...$response_closure): ApiClient
    {
        $handler = new MockHandler($response_closure);

        return new ApiClient(
            options: [ 'handler' => $handler ]
        );
    }
}
