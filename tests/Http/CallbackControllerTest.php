<?php

declare(strict_types=1);

namespace BmPlatform\Bitrix24\Tests\Http;

use BmPlatform\Bitrix24\EventHandler;
use BmPlatform\Bitrix24\Http\CallbackController;
use BmPlatform\Bitrix24\Tests\TestCase;
use Illuminate\Container\Container;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Mockery;

class CallbackControllerTest extends TestCase
{
    public function testInvoke(): void
    {
        $response_factory = Mockery::mock(ResponseFactory::class);
        $response_factory->expects('make')
            ->with('', 200, [])
            ->andReturn(new Response(''));

        $container = Mockery::mock(Container::class);
        $container->expects('make')
            ->with(ResponseFactory::class, [])
            ->once()
            ->andReturn($response_factory);

        Container::setInstance($container);

        $event_handler = Mockery::mock(EventHandler::class);
        $event_handler->expects('__invoke')->once();

        $controller = new CallbackController();
        $response = $controller(
            new Request(['payload' => 'payload']),
            $event_handler
        );

        $this->assertEquals('', $response->getContent());
    }
}
