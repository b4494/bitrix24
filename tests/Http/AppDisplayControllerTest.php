<?php

namespace BmPlatform\Bitrix24\Tests\Http;

use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\AppHandler;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24Bot;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24User;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use BmPlatform\Bitrix24\Http\AppController;
use BmPlatform\Bitrix24\Http\AppInstallController;
use BmPlatform\Bitrix24\Tests\TestCase;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class AppDisplayControllerTest extends TestCase
{
    public function testAppDisplays(): void
    {
        $request = \Mockery::mock(Request::class);
        $request->expects('input')->andReturn('input');

        $credentialsRepo = \Mockery::mock(CredentialsRepo::class);
        $credentialsRepo->expects('has')->with('app_code')->andReturn(true);
        $credentialsRepo->expects('getByCodeOrFail')->with('app_code')->andReturn(new Bitrix24AppCredentials(
            'app_code', 'clientId', 'clientSecret', 'botCode', 'botName',
        ));

        $b24Operator = \Mockery::mock(Bitrix24User::class);
        $b24Operator->expects('toOperator')->andReturn($operator = \Mockery::mock(Operator::class));

        $apiCommands = \Mockery::mock(ApiCommands::class);
        $apiCommands->expects('appInfo')->andReturn(new Bitrix24AppInfo('id', 'app_code', '1', 'L', false, null, true, 'ru'));
//        $apiCommands->expects('getBots')->andReturn([
//            new Bitrix24Bot(123, 'botCode', 'botName'),
//        ]);
        $apiCommands->expects('currentUser')->andReturn($b24Operator);

        $payload = \Mockery::mock(AppDisplayPayload::class, [
            new OAuthAccessToken('accessToken', 0, 'endpoint', 'refreshToken'),
            'memberId', 'domain', 'appToken', 'status', 'lang', 'placement', null,
        ]);
        $payload->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();

        $controller = \Mockery::mock(AppController::class);
        $controller->makePartial()->shouldAllowMockingProtectedMethods();
        $controller->expects('getPayload')->with($request)->andReturn($payload);

        $appHandler = \Mockery::mock(AppHandler::class);
        $appHandler->expects('getApiCommands')->andReturn($apiCommands);

        $appInstance = \Mockery::mock(AppInstance::class);
        $appInstance->expects('getHandler')->andReturn($appHandler);

        $hub = \Mockery::mock(Hub::class);
        $hub->expects('findAppById')->with('b24', 'memberId:botCode')->andReturn($appInstance);
        $hub->expects('getAuthToken')->with($appInstance, $operator)->andReturn('auth_token');
        $hub->expects('webUrl')->with('auth_token')->andReturn('redirect_url');

        Log::expects('debug');

        $redirectService = \Mockery::mock();
        $redirectService->expects('to');

        $app = \Mockery::mock(\Illuminate\Contracts\Container\Container::class);
        $app->expects('make')->with('redirect', [])->andReturn($redirectService);

        Container::setInstance($app);

        $controller($request, $hub, $credentialsRepo);
    }
}