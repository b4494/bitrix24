<?php

namespace BmPlatform\Bitrix24\Tests\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Bitrix24\ApiCommands;
use BmPlatform\Bitrix24\CredentialsRepo;
use BmPlatform\Bitrix24\Entities\Bitrix24\Bitrix24AppInfo;
use BmPlatform\Bitrix24\Entities\OAuth\Bitrix24AppCredentials;
use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use BmPlatform\Bitrix24\Http\AppInstallController;
use BmPlatform\Bitrix24\Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class AppInstallControllerTest extends TestCase
{
    public function testIntegrates(): void
    {
        $request = \Mockery::mock(Request::class);

        $credentialsRepo = \Mockery::mock(CredentialsRepo::class);
        $credentialsRepo->expects('has')->with('app_code')->andReturn(true);
        $credentialsRepo->expects('getByCodeOrFail')->with('app_code')->andReturn(new Bitrix24AppCredentials(
            'app_code', 'clientId', 'clientSecret', 'botCode', 'botName',
        ));

        $apiCommands = \Mockery::mock(ApiCommands::class);
        $apiCommands->expects('appInfo')->andReturn(new Bitrix24AppInfo('id', 'app_code', '1', 'L', false, null, false, 'ru'));
        $apiCommands->expects('createBot')->andReturn(123);

        $payload = \Mockery::mock(AppDisplayPayload::class, [
            new OAuthAccessToken('accessToken', 0, 'endpoint', 'refreshToken'),
            'memberId', 'domain', 'appToken', 'status', 'lang', 'placement', null,
        ]);
        $payload->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();

        $hub = \Mockery::mock(Hub::class);
        $hub->expects('integrate');
        $hub->expects('getAuthToken')->andReturn('auth_token');
        $hub->expects('webUrl')->with('auth_token')->andReturn('redirect_url');

        $controller = \Mockery::mock(AppInstallController::class);
        $controller->makePartial()->shouldAllowMockingProtectedMethods();
        $controller->expects('getPayload')->with($request)->andReturn($payload);

        \Illuminate\Support\Facades\View::expects('make')->with('b24::install', [ 'redirectUrl' => 'redirect_url' ]);
        Config::expects('get')->with('bitrix24.bot.email')->andReturn('email');
        Config::expects('get')->with('bitrix24.bot.url')->andReturn('url');

        $controller($request, $credentialsRepo, $hub);
    }
}