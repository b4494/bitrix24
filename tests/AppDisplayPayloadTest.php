<?php

namespace BmPlatform\Bitrix24\Tests;

use BmPlatform\Bitrix24\Entities\OAuth\OAuthAccessToken;
use BmPlatform\Bitrix24\Entities\Webhook\AppDisplayPayload;
use Illuminate\Http\Request;

class AppDisplayPayloadTest extends TestCase
{
    public function testConverts(): void
    {
        $request = \Mockery::mock(Request::class);
        $request->expects('input')->andReturn($this->getWebhookPayload('OPENAPPINSTALL'));

        $this->assertEquals(new AppDisplayPayload(
            new OAuthAccessToken(
                'a51be26300612c00003122e400000013000007be61f6d9c9f1df3eed277804aa01cb98',
                time() + 3600,
                'https://bot-marketing.bitrix24.ru/rest/',
                '959a096400612c00003122e400000013000007f7825e1ff24643b381865b9c1ddc6042',
            ),
            memberId: '9f9c63e8c5980b426e0157317330cb64',
            domain: 'bot-marketing.bitrix24.ru',
            appToken: '',
            status: 'L',
            lang: 'ru',
            placement: 'DEFAULT',
            placementOptions: [
                'IFRAME' => 'Y',
                'IFRAME_TYPE' => 'SIDE_SLIDER',
                'any' => '41/'
            ],
        ), AppDisplayPayload::fromRequest($request));
    }
}