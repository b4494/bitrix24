<?php

use BmPlatform\Abstraction\Enums\MessengerType as InternalMessengerTypeEnum;
use BmPlatform\Bitrix24\Enums\ExternalMessengerTypeEnum;

return [
    'applications' => json_decode(env('BITRIX24_MODULE_INTEGRATIONS', '[]'), true),

    'bot' => [
        'code' => env('BITRIX24_BOT_CODE', 'bot_marketing_chat_bot'),
        'name' => env('BITRIX24_BOT_NAME', 'BotMarketing Bot'),
        'email' => env('BITRIX24_BOT_EMAIL', 'info@bot-marketing.com'),
        'url' => env('BITRIX24_BOT_URL', 'https://bot-marketing.com'),
    ],

    'title' => 'Bitrix24',
    'logo' => '<svg data-logo="" width="195" height="35" viewBox="0 0 195 35" xmlns="http://www.w3.org/2000/svg"><g fill="none"><path d="M132.5 25.4h17.6v-3.9h-11.6c1.6-6.4 11.4-7.8 11.4-14.9 0-3.8-2.6-6.6-8.1-6.6-3.4 0-6.4 1-8.4 2l1.2 3.6c1.8-.9 3.9-1.7 6.5-1.7 2 0 3.9.9 3.9 3.2 0 5.2-11.5 5.6-12.5 18.3zm18.1-6.2h11.3v6.2h4.5v-6.2h3.8v-3.8h-3.8V0h-3.3l-12.5 16.2v3zm5.1-3.6 6.4-8.6c0 .7-.2 2.9-.2 4.9v3.6h-3c-.9 0-2.6.1-3.2.1z" fill="#005893"></path><path d="M4.7 21.6v-7.9h1.9c1.7 0 3.1.2 4.1.8 1 .6 1.6 1.6 1.6 3.2 0 2.7-1.6 3.9-5.4 3.9H4.7zM.1 25.4h6.7c7.5 0 10.2-3.3 10.2-7.9 0-3.1-1.3-5.2-3.6-6.4-1.8-1-4.1-1.3-6.9-1.3H4.7v-6h10.1L16 .1H0v25.3h.1zm20 0h4.4l5.7-8.2c1.1-1.5 1.9-3 2.4-3.8h.1c-.1 1.1-.2 2.5-.2 3.9v8H37v-18h-4.4l-5.7 8.2c-1 1.5-1.9 3-2.4 3.8h-.1c.1-1.1.2-2.5.2-3.9v-8h-4.5v18zm24.6 0h4.6V11.1h5.4l1.2-3.8H39.2v3.8h5.5v14.3zm12.8 9.1H62v-9.1c.9.3 1.8.4 2.8.4 5.7 0 9.4-3.9 9.4-9.5 0-5.8-3.4-9.5-9.9-9.5-2.5 0-4.9.5-6.9 1.1v26.6h.1zm4.5-13V10.9c.7-.2 1.3-.3 2.1-.3 3.3 0 5.4 1.8 5.4 5.7 0 3.5-1.7 5.7-5.1 5.7-.9 0-1.6-.2-2.4-.5zm14.9 3.9h4.4l5.7-8.2c1.1-1.5 1.9-3 2.4-3.8h.1c-.1 1.1-.2 2.5-.2 3.9v8h4.5v-18h-4.4l-5.7 8.2c-1 1.5-1.9 3-2.4 3.8h-.1c.1-1.1.2-2.5.2-3.9v-8h-4.5v18zm20.5 0h4.6v-7.5h2.7c.5 0 1 .5 1.6 1.7l2.3 5.8h4.9l-3.3-6.9c-.6-1.2-1.2-1.9-2.1-2.2v-.1c1.5-.9 1.7-3.5 2.6-4.8.3-.4.7-.6 1.3-.6.3 0 .7 0 1 .2V7.1c-.5-.2-1.4-.3-1.9-.3-1.6 0-2.6.6-3.3 1.6-1.5 2.2-1.5 6-3.7 6H102V7.3h-4.6v18.1zm26.3.4c2.5 0 4.8-.8 6.2-1.8l-1.3-3.1c-1.3.7-2.5 1.2-4.2 1.2-3.1 0-5.1-2-5.1-5.7 0-3.3 2-5.9 5.4-5.9 1.8 0 3.1.5 4.4 1.4V8c-1-.6-2.6-1.2-4.9-1.2-5.4 0-9.6 4-9.6 9.7 0 5.2 3.2 9.3 9.1 9.3z" fill="#0BBBEF"></path><path d="M185.1 19.2c4.9 0 8.9-4 8.9-8.9s-4-8.9-8.9-8.9-8.9 4-8.9 8.9c.1 4.9 4 8.9 8.9 8.9z" stroke-width="1.769" stroke="#005893"></path><path d="M190.7 10.3h-4.9V5.4h-1.3v6.2h6.2v-1.3z" fill="#005893"></path></g></svg>',

    'events' => [
        'outboxSent' => true,
    ],

    'custom_events' => [
        [
            'id' => 'botJoin',
            'title' => 'Бот добавлен в чат',
            'icon' => 'person-plus',
        ]
    ],

    'custom_actions' => [
        [
            'id' => 'restApiCall',
            'title' => 'REST API',
            'icon' => 'house-gear',
            'args' => [
                'type' => 'object',
                'properties' => [
                    'action' => [
                        'type' => 'string',
                        'title' => 'Метод',
                        'example' => 'crm.lead.add',
                    ],

                    'query' => [
                        'type' => 'keyValue',
                        'title' => 'Параметры',
                    ]
                ],
                'required' => [ 'action' ],
            ],

            'result' => [
                'type' => 'object',
                'properties' => [
                    'data' => [ 'type' => 'mixed', 'title' => 'Результат запроса' ],
                    'status' => [ 'type' => 'text', 'title' => 'Статус' ],
                    'message' => [ 'type' => 'text', 'title' => 'Текст ошибки' ],
                ],
            ],
        ],

        [
            'id' => 'leave',
            'title' => 'Покинуть чат',
            'icon' => 'box-arrow-right',
        ],

        [
            'id' => 'publishFiles',
            'title' => 'Опубликовать файлы',
            'icon' => 'share',
            'args' => [
                'type' => 'object',
                'properties' => [
                    'files' => [
                        'type' => 'text',
                        'title' => 'Файлы',
                        'description' => 'Укажите поле `showUrl` строкой, либо объект с полем `showUrl`, либо массив таких объектов.'
                    ],
                ]
            ],

            'result' => [
                'type' => 'object',
                'properties' => [
                    'urls' => [ 'type' => 'mixed', 'title' => 'Массив ссылок на файлы' ],
                    'errors' => [ 'type' => 'mixed', 'title' => 'Массив ошибок по каждому файлу' ],
                ],
            ],
        ]
    ],

    'actions' => [
        'send_system_message' => [
            'supported' => true,
            'max_length' => 4000,
        ],
    ],

    'features' => [
        'operators' => [
            'read' => true,
            'transfer' => true,
        ],

        // TODO: implement open lines as operator groups
//        'operatorGroups' => [
//            'read' => false,
//            'transfer' => false,
//        ],

//        'tags' => [
//            'read' => false,
//            'asStrings' => false,
//            'attach' => false,
//            'detach' => false,
//            'attachForTicket' => false,
//            'detachForTicket' => false,
//        ],
    ],

    'data' => [
        'contact' => [
            'phone'       => false,
            'email'       => false,
            'avatar'      => false,
            'messengerId' => false,
        ],

        'chat' => [
            'messengerId' => true,
        ],

        'operator' => [
            'phone'  => true,
            'email'  => true,
            'avatar' => true,
        ],
    ],

    'messenger_types' => [
        'default' => [
            'name' => '',
            'text' => [
                'max_length' => 4096,
            ],
        ],

        'whatsappbytwilio' => [
            'extends'       => 'default',
            'name'          => 'WhatsApp by Twilio',
            'internal_type' => 'whatsapp',
        ],

        'mit_yousapp_subscribe_connector' => [
            'extends'       => 'default',
            'name'          => 'Yousapp',
            'internal_type' => 'whatsapp',
        ],

        'telegrambot' => [
            'extends'       => 'default',
            'name'          => 'Telegram',
            'internal_type' => 'telegram',
        ],

        'avito' => [
            'extends' => 'default',
            'name'    => 'Avito',
        ],

        'viber' => [
            'extends'       => 'default',
            'name'          => 'Viber',
            'internal_type' => 'viber_public',
        ],

        'imessage' => [
            'extends' => 'default',
            'name'    => 'Apple Messages for Business',
        ],

        'vkgroup' => [
            'extends'       => 'default',
            'name'          => 'ВКонтакте',
            'internal_type' => 'vk',
        ],

        'ok' => [
            'extends' => 'default',
            'name'    => 'Одноклассники',
        ],

        'facebook' => [
            'extends'       => 'default',
            'name'          => 'Facebook: Сообщения',
            'internal_type' => 'facebook',
        ],

        'facebookcomments' => [
            'extends' => 'default',
            'name'    => 'Facebook: Комментарии',
        ],

        'fbinstagramdirect' => [
            'extends' => 'default',
            'name'    => 'Instagram Direct',
        ],

        'whatsappbyedna' => [
            'extends'       => 'default',
            'name'          => 'Edna.ru',
            'internal_type' => 'whatsapp',
        ],

        'fos_green_api' => [
            'extends'       => 'default',
            'name'          => 'green-api.com',
            'internal_type' => 'whatsapp',
        ],

        'livechat' => [
            'extends' => 'default',
            'name'    => 'Онлайн-чат',
        ],

        'ank_chats_app24_whatsapp' => [
            'extends' => 'default',
            'name' => 'ChatApp',
            'internal_type' => 'whatsapp',
        ],

        'network' => [
            'extends' => 'default',
            'name' => 'Битрикс24.Network',
        ],

        'olchat_wa_connector' => [
            'extends' => 'default',
            'name' => 'WhatsApp',
            'internal_type' => 'whatsapp',
        ],

        'c2d_1gt_connector' => [
            'extends' => 'default',
            'name' => 'Chat2Desk',
        ],

        'cls' => [
            'extends' => 'default',
            'name' => '',
            'text' => [
                'max_length' => 1000,
            ],
        ],

        'bm_cls_avito' => [
            'extends' => 'cls',
            'name' => 'Avito (BotMarketing)',
        ],

        'bm_cls_youla' => [
            'extends' => 'cls',
            'name' => 'Youla (BotMarketing)',
        ],

        'bm_cls_drom' => [
            'extends' => 'cls',
            'name' => 'Drom (BotMarketing)',
        ],

        'wz_whatsapp' => [
            'extends' => 'default',
            'name' => 'WAZZUP: WhatsApp',
            'internal_type' => 'whatsapp',
        ],

        'wz_instagram' => [
            'extends' => 'default',
            'name' => 'WAZZUP: Instagram',
            'internal_type' => 'instagram',
        ],

        'wz_telegram' => [
            'extends' => 'default',
            'name' => 'WAZZUP: Telegram',
            'internal_type' => 'telegram',
        ],

        'wz_avito' => [
            'extends' => 'default',
            'name' => 'WAZZUP: Avito',
        ],

        'bitrix_whatcrm_net' => [
            'extends' => 'default',
            'name' => 'Whatcrm',
            'internal_type' => 'whatsapp',
        ],
    ],

    'dynamic_messenger_types' => [
        '/^(?<type>wz_(whatsapp|instagram|telegram|avito))_[a-z0-9]*$/',
        '/^(?<type>bitrix_whatcrm_net)_\d+$/',
    ],
];
